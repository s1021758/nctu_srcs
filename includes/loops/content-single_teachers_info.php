<?php
/*
The Single Posts Loop  (專任師資詳細內文)
=====================
*/

?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/page.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/homepage.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(0)").addClass('nav_active');
      $ (".sidebarmenu a:eq(1)").removeClass('a_show')
      $ (".sidebarmenu a:eq(1)").addClass('sidebarmenu_active')

      function clean(){
        $("#line1").removeClass('click_style');
        $("#line1").addClass('normal_style');
        $("#line2").removeClass('click_style');
        $("#line2").addClass('normal_style');
        $("#line3").removeClass('click_style');
        $("#line3").addClass('normal_style');
        $("#line4").removeClass('click_style');
        $("#line4").addClass('normal_style');
        $("#line5").removeClass('click_style');
        $("#line5").addClass('normal_style');
    }

      $("#item1").click(function(){
        clean();
        $("#line1").removeClass('normal_style');
        $("#line1").addClass('click_style');
      });
      $("#item2").click(function(){
        clean();
        $("#line2").removeClass('normal_style');
        $("#line2").addClass('click_style');
      });
      $("#item3").click(function(){
        clean();
        $("#line3").removeClass('normal_style');
        $("#line3").addClass('click_style');
      });
      $("#item4").click(function(){
        clean();
        $("#line4").removeClass('normal_style');
        $("#line4").addClass('click_style');
      });
      $("#item5").click(function(){
        clean();
        $("#line5").removeClass('normal_style');
        $("#line5").addClass('click_style');
      });
});

  </script>
  <style>
    @media(max-width: 1025px){
      .mobile_title_lines{
      }
      #btn_area{
        line-height: 3vh;
      }
      .mobile_container{
        display: flex;
        flex-direction: column;
      }
      .float_area{
        display: flex !important;
        flex-direction: column;
        justify-content: center;
      }
      .photo-teacher{
        margin: 0 !important;
        display: flex !important;
        justify-content: center;
        margin-bottom: 2.5vh !important;
      }
      .right-side-pro-detail{
        width: 80vw !important;
      }
      .right-side-pro-detail div{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 500 !important;
        font-size: 1.25em !important;
        line-height: 2vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(20, 20, 20, 1) !important;
        margin-bottom: 2.5vh !important;
      }
      .right-side-pro-detail div p{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 1.8em !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        margin-bottom: 2.5vh !important;
        width: 80vw;
      }
      .right-side-pro-detail div p br{
        display: none;
      }
      .professor_name_block{
        width: 80vw !important;
      }
      .professor_position{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 500 !important;
        font-size: 1.25em !important;
        line-height: 2vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(20, 20, 20, 1) !important;
        margin-bottom: 1.5vh !important;
        text-align: center;
      }
      .professor_name{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 600 !important;
        font-size: 1.8em !important;
        letter-spacing: 0.3em !important;
        color: rgba(50, 50, 50, 1) !important;
        margin-bottom: 1.5vh !important;
        text-align: center;
      }
      .professor_info_block{
        height: auto;
        margin-bottom: 5vh;
      }
      .pro_blocks{
        height: auto;
/*        position: relative;
        float:right;
        top:-7.5vh;*/
      }
      .pro_blocks ul{
        height: auto;
        display: flex !important;
        width: 90vw;
        justify-content: space-around;
        position: relative;
        right: 5vw;
      }
      .pro_blocks ul li{
        width: auto;
      }
      .click_style{
        display: none !important;
      }
      .normal_style{
        display: none !important;
      }
      .teacher_profile{
        
      }
      hr{
        margin-bottom: 1.5vh !important;
      }
      li a{
        background: rgba(199, 199, 199, 1);
        color: rgba(255 , 255 , 255 , 1);
      }
    }
  </style>
</head>
<?php get_template_part('includes/phone-list'); ?>
<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar'); ?>


<body>
    <div class="main">
      <div class="title_block">
        <div class="mobile_title_lines"></div>
        <div class="botton_container">
            <a href="http://127.0.0.1/wp/full-time-teacher/">
              <img class="botton1"src="../../../../wp-content/themes/nctu_srcs/images/mobile/btn/btn_personalweb_dark_grey.svg">
            </a> 
            <img  onclick="show_menu()" class="botton1" src="../../../../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
          </div>
      </div>
      <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <a href="#" class="button" style="margin-top:3vh; margin-bottom: 3vh;">
          <div id="btn_area" class="button_style_top">TOP</div>
        </a>
    <div class="mobile_container">
      <div class="float_area" style="display: inline-block;">
        <div class="photo-teacher" style="margin-right: 1.5vw; width: auto; display: inline-block; vertical-align: top;">

        <?php 

        $image = get_field('teacher_photo');

        if( !empty($image) ): ?>

          <img style="width:200px; height:auto;" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

        <?php endif; ?>

      </div>
      
        <div class="professor_name_block" style="width: 15.5vw; display: inline-block;  vertical-align: top;">
          <div class="professor_position" style="height: 2.75vh; margin-bottom: 1vh;">
            <?php $teacher_position = get_field( "teacher_position" ); 
              if( $teacher_position ){
                echo $teacher_position;
              }
            ?>
          </div>
          <div class="professor_name" style="margin-bottom: 11vh;">
            <?php the_title(); ?>  <!-- teacher's name is equal to the article title -->
          </div>
          <div class="professor_info_block">
            <!-- <HR width="30px" size="1px" > -->
            <div class="pro_blocks" style="display: inline-block; margin-left: 0.5vw;margin-top: -1vh;">
              <ul style="list-style: none;">
                <li id="item1"><div id="line1" class="click_style"></div><a href="#education">學經歷</a></li>
                <li id="item2"><div id="line2" class="normal_style"></div><a href="#profile">簡介</a></li>
                <li id="item3"><div id="line3" class="normal_style"></div><a href="#professional">學術專長領域</a></li>
                <li id="item4"><div id="line4" class="normal_style"></div><a href="#teacher_works">著作</a></li>
                <li id="item5"><div id="line5" class="normal_style"></div><a href="#research_project">研究計畫</a></li>
              </ul>
            </div>
          </div>

        </div>
      </div>



      <div class="right-side-pro-detail" style="display: inline-block;float: right; margin-right: 2vw;  width: 40vw;">

        <div id="education" class="teacher_education" style="height: 2vh;margin-bottom: 3vh;">
          <?php $teacher_education = get_field( "teacher_education" ); 
            if( $teacher_education ){
              echo $teacher_education;
            }
          ?>
        </div>
        <HR width="15px" size="1px" style="margin-bottom: 2.5vh;">
        <div class="teacher_profile" style="margin-bottom: 2.5vh;">
          <p>
            <?php $teacher_contact = get_field( "teacher_contact" ); 
              if( $teacher_contact ){
                echo $teacher_contact;
              }
            ?>
          </p>
          
        </div>
        <HR width="15px" size="1px" style="margin-bottom: 2.5vh;">
        <div id="profile" class="profession_intro" style="    margin-bottom: 1.5vh;">簡介</div>
        <div class="teacher_detail_content" style="margin-bottom: 2.5vh;">
          <p><?php $teacher_profile = get_field( "teacher_profile" ); 
              if( $teacher_profile ){
                echo $teacher_profile;
              }
            ?></p>
        </div>
        <HR width="15px" size="1px" style="margin-bottom: 2.5vh;">
        <div id="professional" class="profession_area" style="    margin-bottom: 1.5vh;">學術專長領域</div>
        <div class="professional_area" style="margin-bottom: 2.5vh;">
          <p>
            <?php $teacher_professional_area = get_field( "teacher_professional_area" ); 
              if( $teacher_professional_area ){
                echo $teacher_professional_area;
              }
            ?>
          </p>
          
        </div>
        <HR width="15px" size="1px" style="margin-bottom: 2.5vh;">
        <div id="teacher_works" class="profession_intro" style="    margin-bottom: 1.5vh;">著作</div>
        <div class="teacher_detail_content" style="margin-bottom: 2.5vh;">
          <p>
            <?php $teacher_works = get_field( "teacher_works" ); 
              if( $teacher_works ){
                echo $teacher_works;
              }
            ?>
          </p>
          
        </div>
        <HR width="15px" size="1px" style="margin-bottom: 2.5vh;">
        <div id="research_project" class="profession_intro" style="    margin-bottom: 1.5vh;">研究領域與計畫</div>
        <div class="teacher_detail_content" style="margin-bottom: 2.5vh;">         
          <p><?php $teacher_research_projects = get_field( "teacher_research_projects" ); 
              if( $teacher_research_projects ){
                echo $teacher_research_projects;
              }
            ?></p>
        </div>
        
      </div>
    </div>
      <?php endwhile; ?>
    <?php endif; ?>
    </div>
</body>
