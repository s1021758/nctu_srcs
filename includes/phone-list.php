<?php
/*
 * Template Name: homepage
 */
?>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <title><?php wp_title('•', true, 'right'); bloginfo('name'); ?></title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/page.css" type="text/css" />
  <script src="https://use.typekit.net/hgf1mzq.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script>



  <script type="text/javascript" charset="utf-8">

  function show_menu()
    {
      document.getElementById('menu').setAttribute("class", "menu");
      /*for template-student-download.php*/
      if($(".circle_container").css("display") == 'flex'){
        $(".circle_container").hide();
      }
    }
  function close_phone()
    {
      document.getElementById('close_phone').setAttribute("class", "close");
    }
  function close_newpost()
    {
      document.getElementById('new_post').setAttribute("class", "close");
    }
  function close_menu()
    {
      document.getElementById('menu').setAttribute("class", "close");
      /*for template-student-download.php*/
      if($(".circle_container").css("display") == 'none'){
        $(".circle_container").show();
      }
    }
  function show_newpost()
    {
      document.getElementById('new_post').setAttribute("class", "new_post");
    }
  function show_menu1()
    {
      document.getElementById('menu1').setAttribute("class", "menu");
    }
  function show_menu2()
    {
      document.getElementById('menu2').setAttribute("class", "menu");
    }
  function show_menu3()
    {
      document.getElementById('menu3').setAttribute("class", "menu");
    }
  function show_menu4()
    {
      document.getElementById('menu4').setAttribute("class", "menu");
    }
  function show_menu5()
    {
      document.getElementById('menu5').setAttribute("class", "menu");
    }
  function show_menu6()
    {
      document.getElementById('menu6').setAttribute("class", "menu");
    } 
  function close_menu1()
    {
      document.getElementById('menu1').setAttribute("class", "close");
    }
  function close_menu2()
    {
      document.getElementById('menu2').setAttribute("class", "close");
    }
  function close_menu3()
    {
      document.getElementById('menu3').setAttribute("class", "close");
    }
  function close_menu4()
    {
      document.getElementById('menu4').setAttribute("class", "close");
    }
  function close_menu5()
    {
      document.getElementById('menu5').setAttribute("class", "close");
    }
  function close_menu6()
    {
      document.getElementById('menu6').setAttribute("class", "close");
    }
  function close_sidebarmenu()
    {
      document.getElementById('menu6').setAttribute("class", "close");
    }   
  </script>


  <style type="text/css">
    @media screen and (max-width: 1024px){
      #menu{
        
      }
      #close_phone{

      }
      .menu{        
        float: left;
        width: 100vw;
        height: 100vh;
        margin-bottom: 0;
        background-image: url("../wp-content/themes/nctu_srcs/images/mobile/index_bg_1.jpg") !important;
        background-repeat: no-repeat;
        background-size:100vw 100vh;
      }
      .menu-class{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 600;
        font-size: 1.7em;
        letter-spacing: 0.1em;
        color: rgba(120, 120, 120, 1);
        height: 3.125vh;
        margin-bottom: 3.5vh;
        margin-left: 10vw;
      }
      .menu-text{
        height: 3.125vh;
        margin-bottom: 3.5vh;
        margin-left: 10vw;
      }
      .menu-text a{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        color: rgba(120, 120, 120, 0.8);
        letter-spacing: 0.1em;
        font-weight: 600;
        font-size: 1.4em;
      }
      .menu-button-1{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400;
        font-size: 1em;
        letter-spacing: 0.1em;
        color: rgba(120, 120, 120, 1);
        text-align: right;
        width: 94vw;
        height: 1.8vh;
        margin-bottom: 1.5vh;
      }
      .menu-button-2{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400;
        font-size: 1em;
        letter-spacing: 0.1em;
        color: rgba(120, 120, 120, 1);
        text-align: right;
        float: right;
        margin-left: 10vw;
      }
      .menu-block{
        margin-top: 37.25vh;
      }
      .close{
        display: none;
      }
      .menu-btn{
        float: right;
        width: 6.25vw;
        height: 6.25vw;
        margin-right:6vw;
        margin-top: 3.75vh; 
        opacity:0.5;

      }
      .header{
        display: none;
      }
      /**menu裡面每個小項目的ID**/
      #menu1{

      }
      #menu2{

      }
      #menu3{

      }
      #menu4{

      }
      #menu5{

      }
      #menu6{

      }





    }
    @media screen and (min-width: 1025px){
      .menu-btn{
        display: none;
        
      }
      .close{
        display: none;
      }
      .phone-menu{
        display: none;
      }
    }    
  </style>
</head>



<body>
    <!--phone_menu-->
    <div id="menu" class="close">

        <div class="menu-btn">
          <img onclick="close_menu(),show_newpost()" src="../wp-content/themes/nctu_srcs/images/mobile/btn/menu-close.png">
        </div>
        <div style="height: 11vh;"></div> 
        <div onclick="close_menu(),show_menu1()" class="menu-class">關於本所</div>
        <div onclick="close_menu(),show_menu2()" class="menu-class">研究發展</div>
        <div onclick="close_menu(),show_menu3()" class="menu-class">課程規劃</div>
        <div onclick="close_menu(),show_menu4()" class="menu-class">招生</div>
        <div onclick="close_menu(),show_menu5()" class="menu-class">學術交流</div>
        <div onclick="close_menu(),show_menu6()" class="menu-class">學生專區</div>
        <div class="menu-block">
          <div class="menu-button-1">國立陽明交通大學 社會與文化研究所</div>
          <div class="menu-button-1">
            <div class="menu-button-2">Tel: 03-5131593</div>
            <div class="menu-button-2">Fax: 03-5734450</div>
          </div>
          <div class="menu-button-1">30010 新竹市大學路1001號 人社二館二樓212室</div>
        </div>
      </div>
  <!--點各細項後顯示的選單-->
      <div id="menu1" class="close">
        <div class="menu-btn">
          <img onclick="close_menu1(),show_menu()" src="../wp-content/themes/nctu_srcs/images/mobile/btn/menu-close.png">
        </div>
        <div style="height: 11vh;"></div>
        <div class="menu-class">關於本所</div> 
        <div class="menu-text"><a href="<?php echo site_url(); ?>/aboutus/">關於本所</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/full-time-teacher/">專任師資</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/adjunct-teacher/">合聘師資</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/guest-professor/">客座教授</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/administration-staff/">行政人員</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/current-student/">在校生</a></div>
      </div>

      <div id="menu2" class="close">
        <div class="menu-btn">
          <img onclick="close_menu2(),show_menu()" src="../wp-content/themes/nctu_srcs/images/mobile/btn/menu-close.png">
        </div>
        <div style="height: 11vh;"></div> 
        <div class="menu-class">研究發展</div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/research-direction/">研究方向</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/teachers-works/">教師著作</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/research-proposal/">研究計畫</a></div>
      </div>

      <div id="menu3" class="close">
        <div class="menu-btn">
          <img onclick="close_menu3(),show_menu()" src="../wp-content/themes/nctu_srcs/images/mobile/btn/menu-close.png">
        </div>
        <div style="height: 11vh;"></div> 
        <div class="menu-class">課程規劃</div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/present-course-first-semester/">本學年課程</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/past-course/">歷年課程</a></div>
      </div>

      <div id="menu4" class="close">
        <div class="menu-btn">
          <img onclick="close_menu4(),show_menu()" src="../wp-content/themes/nctu_srcs/images/mobile/btn/menu-close.png">
        </div>
        <div style="height: 11vh;"></div>
        <div class="menu-class">招生</div> 
        <div class="menu-text"><a href="<?php echo site_url(); ?>/feature/">社文所特色</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/master/">碩士班</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/doctor/">博士班</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/overseas/">僑生港澳生</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/chinese/">陸生</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/foreign/">外籍生</a></div>
      </div>

      <div id="menu5" class="close">
        <div class="menu-btn">
          <img onclick="close_menu5(),show_menu()" src="../wp-content/themes/nctu_srcs/images/mobile/btn/menu-close.png">
        </div>
        <div style="height: 11vh;"></div> 
        <div class="menu-class">學術交流</div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/academic-cooperation/">國際合作</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/keynote-speech/">專題演講</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/workshop/">研討會工作坊</a></div>
      </div>

      <div id="menu6" class="close">
        <div class="menu-btn">
          <img onclick="close_menu6(),show_menu()" src="../wp-content/themes/nctu_srcs/images/mobile/btn/menu-close.png">
        </div>
        <div style="height: 11vh;"></div> 
        <div class="menu-class">學生專區</div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/rules/">修業規章</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/thesis/">畢業論文</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/dissertation-award/">論文獲獎</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/other-award/">其他獲獎訊息</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/intership-exchange-record/">見習交換紀錄</a></div>
        <div class="menu-text"><a href="<?php echo site_url(); ?>/student-download/">學生下載</a></div>

    </div>

    <!--phone_menu_end-->


</body>

