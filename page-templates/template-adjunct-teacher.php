<?php
/*
 * Template Name: adjunct-teacher
 */
?>
<?php get_template_part('includes/phone-list'); ?>

<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>
  
    $( document ).ready(function() {
      $ ("#top-menu a:eq(0)").addClass('nav_active');
      $ (".sidebarmenu a:eq(2)").removeClass('a_show')
      $ (".sidebarmenu a:eq(2)").addClass('sidebarmenu_active')
});

  </script>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <style>
      .teacher-right-line{
        display: none;
      }
      .home-botton{
        display: none;
      }
      .teacher-name-container{
        display: table;
      }
      .teacher-name-container span{
        width: auto;
        padding-right: 1.5vw;
        display: table-cell;
      }
    @media(max-width: 1024px){
      body{
        position: absolute;
        top: 0;
        left: 0;
      }
      .sidebarmenu{
        height: 56.75vh !important;
      }
      .container{
        margin-top: 11vh;
      }
      .title_block{
        display: inline-flex;
      }
      .main_long{
        height: 2vh !important;
        width: auto !important;
        margin-right:3vw; 
        position: relative;
        top:50%;
        transform:translateY(-50%);
        padding: 0 !important;
        float: left;
        background-color: transparent !important;
      }
      .main_long font{
        width: auto;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 500;
        font-size: 1.3em;
        letter-spacing: 0.1em;
        color: rgba(20, 20, 20, 1);
        line-height: 0vh;
        position: relative;
        top:50%;
        transform:translateY(-50%);
      }
      .mobile_title_lines{
        display: flex;
        margin-right: 2vw;
        width: auto;
        flex-grow: 1;
      }
      .botton_container{
      }
      .name_block{
        width: 80vw !important;
        display: block !important;
        margin-top:0 !important;
        margin-bottom: 2.5vh !important;
      }
      .teacher-name-container{
        display: flex;
      }
      .teacher-name{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 500 !important;
        font-size: 1.25em !important;
        line-height: 2vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        width: auto !important;
        height: 2vh;
        margin-right: 1vw !important;
        margin-bottom: 1vh;
        display: inline-block;
      }
      .teacher-title{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 2vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        width: auto !important;
        margin-right: 1vw !important;
        margin-bottom: 1vh;
        display: inline-block;
      }
      .teacher-right-line{
        display: flex;
        flex-grow: 1;
        max-height: 1px;
        border: none;
        border-top:1px solid #000;
        width: auto;
        position: relative;
        top:1vh;
      }
      .teacher-school{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 0.8em !important;
        line-height: 1.5vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        width: 70vw !important;
        height: 1.5vh;
        margin-right: 0 !important;
        margin-bottom: 1vh;
        display: inline-block;
      }
      .teacher-study{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 300 !important;
        font-size: 0.7em !important;
        line-height: 1.3em !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        width: 70vw !important;
        display: inline-block;
      }
      .home-botton{
        width: 5.5vw;
        max-width: 34px;
        float: right;
        display: inline-block;
      }
    }
  </style>
</head>

<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar'); ?>
<?php get_template_part('includes/phone-list'); ?>


<div class="container">
  <div class="row">
    <div class="main" >
      <div class="title_block">
        <div class="main_long"><font>合聘師資</font></div>
        <div class="mobile_title_lines"></div>
        <div class="botton_container">
            <a href="<?php echo site_url(); ?>/full-time-teacher/">
              <img class="botton1"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
            </a> 
            <img  onclick="show_menu(),close_newpost()" class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
            <a href="<?php echo site_url(); ?>/guest-professor/">
              <img class="botton2"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_right_dark_grey.svg">
            </a>  
          </div>
      </div>
      


          <div class="name_block" style="width: 71.875vw; margin-top: 4vh; margin-bottom: 1.25vh;">


            <?php $adjunct_teacher = get_post_meta( $post->ID, 'adjunct_teacher', true );
            foreach( $adjunct_teacher as $teacher){?>
              <div class="name_block" style="width:71.875vw; display: inline-flex; margin-top: 1.25vh; margin-bottom: 1.25vh;">
                <div class="teacher-name-container">
                <span class="teacher-name" style="width:10.5vw; margin-right:1.5vw; letter-spacing: 0.5px;"><?php echo $teacher['adjunct_teacher_name']?></span>
                <span class="teacher-title" style="width:11vw; margin-right:1.5vw;"><?php echo $teacher['adjunct_teacher_current_possition']?></span>
                <hr class="teacher-right-line">
                </div>
                <img class="home-botton"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_personalweb_dark_grey.svg">
                <span class="teacher-school" style="width:20vw; margin-right:1.5vw;"><?php echo $teacher['adjunct_teacher_education']?></span>
                <span class="teacher-study" style="width:31.875vw;"><?php echo $teacher['adjunct_teacher_areas_of_expertise']?></span>
              </div>
              <?php

            }?>
          </div>



        <!-- <span style="width:10.5vw; margin-right:1.5vw; letter-spacing: 0.5px;">Thomas Carl Wall</span>
        <span style="width:11vw; margin-right:1.5vw;">台北科大副教授</span>
        <span style="width:20vw; margin-right:1.5vw;">華盛頓大學英美文學博士</span>
        <span style="width:31.875vw;">20世紀思想史、文學批評史、電影理論與批評</span> -->

      <!-- <div class="name_block" style="width:71.875vw; display: inline-flex; margin-top: 1.25vh; margin-bottom: 1.25vh;">
        <span style="width:10.5vw; margin-right:1.5vw;">司黛蕊</span>
        <span style="width:11vw; margin-right:1.5vw;">中研院助研究員</span>
        <span style="width:20vw; margin-right:1.5vw;">美國芝加哥大學人類學博士</span>
        <span style="width:31.875vw;">次文化、民族誌田野</span>
      </div>
      <div class="name_block" style="width:71.875vw; display: inline-flex; margin-top: 1.25vh; margin-bottom: 1.25vh;">
        <span style="width:10.5vw; margin-right:1.5vw;">顏娟英</span>
        <span style="width:11vw; margin-right:1.5vw;">中研院助歷史語言研究員</span>
        <span style="width:20vw; margin-right:1.5vw;">美國哈佛大學藝術史博士</span>
        <span style="width:31.875vw;">歷史語言學研究</span>
      </div>
      <div class="name_block" style="width:71.875vw; display: inline-flex; margin-top: 1.25vh; margin-bottom: 1.25vh;">
        <span style="width:10.5vw; margin-right:1.5vw;">魏德驥</span>
        <span style="width:11vw; margin-right:1.5vw;">助理教授</span>
        <span style="width:20vw; margin-right:1.5vw;">台灣大學哲學博士</span>
        <span style="width:31.875vw;">西洋古典哲學、希臘文化史、中國古典哲學、康德哲學、美學</span>
      </div>
      <div class="name_block" style="width:71.875vw; display: inline-flex; margin-top: 1.25vh; margin-bottom: 1.25vh;">
        <span style="width:10.5vw; margin-right:1.5vw;">何東洪</span>
        <span style="width:11vw; margin-right:1.5vw;">輔仁大學心理系副教授</span>
        <span style="width:20vw; margin-right:1.5vw;">英國藍開斯特大學社會學博士</span>
        <span style="width:31.875vw;">流行音樂研究、青年文化與政策、文化經濟學、當代批判理論、社會科學質性研究</span>
      </div>
      <div class="name_block" style="width:71.875vw; display: inline-flex; margin-top: 1.25vh; margin-bottom: 1.25vh;">
        <span style="width:10.5vw; margin-right:1.5vw;">黃建宏</span>
        <span style="width:11vw; margin-right:1.5vw;">台北藝術大學美術系副教授</span>
        <span style="width:20vw; margin-right:1.5vw;">巴黎第八大學哲學所美學組博士</span>
        <span style="width:31.875vw;">哲學、電影研究、當代藝術思潮</span>
      </div>
      <div class="name_block" style="width:71.875vw; display: inline-flex; margin-top: 1.25vh; margin-bottom: 1.25vh;">
        <span style="width:10.5vw; margin-right:1.5vw;">方天賜</span>
        <span style="width:11vw; margin-right:1.5vw;">清華大學兼任助理教授</span>
        <span style="width:20vw; margin-right:1.5vw;">倫敦政治經濟學院國際關係博士</span>
        <span style="width:31.875vw;">國際關係、印度研究</span>
      </div> -->
      </div>
    </div>

  </div><!-- /.row -->
</div><!-- /.container -->
