<?php
/*
 * Template Name: research-direction
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(1)").addClass('nav_active');
      $ (".sidebarmenu a:eq(0)").removeClass('a_show');
      $ (".sidebarmenu a:eq(0)").addClass('sidebarmenu_active');
});

    
  </script>
  <style type="text/css">
    @media(max-width: 1024px){
      body{
        background-image: url("../wp-content/themes/nctu_srcs/images/cellphone_background.jpg");
        background-size: 100%;
      }
      .sidebarmenu{
        display: none;
      }
      .header{
        display: none;
      }
      .row{
        width:80vw;
        margin-left: 10vw;
      }
      .title_r{
        float: left;
      }
      .title_block{
        margin-top: 11vh;
        padding-bottom: 0vh;
      }
      #content_block{
        margin-top: 0vh !important;
        margin-left: 0vh !important;
        width:80vw !important;
        padding: 0vh 0vw !important;
      }
      .container{
        margin-left: 0vw !important;
        width: 90vw;
        padding-right: 0px;
        padding-left: 0px;
      }

      .direction{
        width: 80vw !important;
        margin-bottom: 0vh !important;
        margin-right: 0vw !important;  
        display: inline-block !important;

      }
      .col-lg-3{
        padding-right: 0 !important;
        padding-left: 0 !important;
      }
      .circle_block{
        width: 31.25vw !important;
        height: 31.25vw !important;
      }
      .circle_left{
        float: left;
      }
      .circle_right{
        float: right;
      } 
      .title_left{
        float: left;
        width:42.5vw;
      } 
      .title_right{
        float: right;
        width:42.5vw;
      }  
      .title_1{
        margin-top: 0vh; 
        width: auto; 
        padding: 0vh;
        width:30vw;
      }
      .direction p{
        font-size: auto !important;
      }


      .research-direction_title{
        margin-top: 0vh !important;
        width:42.5vw !important; 
        padding: 0vh !important; 
        text-align:left !important;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400;
        font-size: 1.25em;
        line-height: 2vh;
        letter-spacing: 0.1em;
        color: rgba(50, 50, 50, 1);
      }
      .research-direction_title p{
        margin-top: 1.5vh !important;
        width:42.5vw !important; 
        padding: 0vh !important; 
        text-align:left !important;
      }
      #content{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 300;
        font-size: 0.8em;
        line-height: 1.4em;
        letter-spacing: 0.1em;
        color: rgba(50, 50, 50, 1);
      }
      .direction hr{
        margin-bottom: 1.5vh !important;
      }
      
        
    } 

    @media(min-width: 1024px){
      .title_block{
        display: none;
      }
      .phone{
        display: none;
      }
      .title_1{
        margin-top: 3.5vh; 
        width: 23vh; 
        padding: 1vh; 
        text-align: -webkit-center;
      }
    }
  </style>      
</head>

<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar-research'); ?>
<?php get_template_part('includes/phone-list'); ?>

<body>
  <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script> -->


<div class="container" style="margin-left: 24vw;">

      <div id="content_block" class="content_phone_block" style="margin-top: -68vh;/*margin-left: 25vw; width:76vw;padding: 3vh 5vw;*/">
          <div class="row">
            
<!--phone title-->
            <div class="title_block">
              <div class="title_r">課程重點</div>
              <div class="mobile_title_lines"></div>
              <div class="botton_container2">
                <img onclick="show_menu()" class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
                <a href="<?php echo site_url(); ?>/teachers-works/"> 
                  <img class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_right_dark_grey.svg">
                </a>
              </div>
            </div> 
            <div class="clear_both"></div>
<!--phone title end-->

            <div class="col-lg-3 col-md-6">
              <div class="direction" style="width: 25vh;margin-bottom: 5vh;  margin-right: 3vw;  display: inline-block; ">
                <div class="circle_block circle_left" style="width: 25vh; height: 25vh; ">
                  <?php
                  $image = get_field('reserch_direction_image_1');
                  if( !empty($image) ): ?>
                    <img id="circle" class="img-fluid" style="border-radius: 12.5vh; box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.4);" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                  <?php endif; ?>
                </div>
                <div class="title_right">
                  <div class="title_1">
                    <p>
                      <?php $research_title1 = get_field( "research_title1" );
                        if( $research_title1 ){
                        echo $research_title1;
                      }
                      ?>
                    </p>
                  </div>
                  <div><HR size="1px" color="#4F4F4F" style="margin-top:1.5vh; margin-bottom:2vh;"></div>
                  <div id="content" >
                    <?php $research_content_1 = get_field( "research_content_1" );
                      if( $research_content_1 ){
                      echo $research_content_1;
                      }
                    ?>
                  </div>
                 </div> 
              </div>
            </div>
            <div class="col-lg-3 col-md-6">
              <div class="direction" style="width: 25vh;margin-bottom: 5vh; margin-right: 3vw; vertical-align: top;display: inline-block;">
                <div class="circle_block circle_right" style="width: 25vh; height: 25vh; ">
                  <?php
                  $image = get_field('reserch_direction_image_2');
                  if( !empty($image) ): ?>
                    <img id="circle" class="img-fluid" style="border-radius: 12.5vh; box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.4);" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                  <?php endif; ?>
                </div>
                <div class="title_left">
                  <div class="title_1">
                    <p>
                      <?php $research_title2 = get_field( "research_title2" );
                        if( $research_title2 ){
                          echo $research_title2;
                        }
                      ?>
                    </p>
                  </div>
                  <div><HR size="1px" color="#4F4F4F" style="margin-top:1.5vh; margin-bottom:2vh;"></div>
                  <div id="content" >
                    <?php $research_content_2 = get_field( "research_content_2" );
                      if( $research_content_2 ){
                        echo $research_content_2;
                      }
                    ?>
                  </div>  
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6">
              <div  class="direction" style="/*width: 25vh;margin-bottom: 5vh; margin-right: 3vw; vertical-align: top;display: inline-block;">
                <div class="circle_block circle_left" style="width: 25vh; height: 25vh; ">
                  <?php
                  $image = get_field('reserch_direction_image_3');
                  if( !empty($image) ): ?>
                    <img id="circle" class="img-fluid" style="border-radius: 12.5vh; box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.4);" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                  <?php endif; ?>
                </div>
                <div class="title_right">
                  <div class="title_1">
                    <p>
                      <?php $research_title3 = get_field( "research_title3" );
                        if( $research_title3 ){
                        echo $research_title3;
                        }
                      ?>
                    </p>
                  </div>
                  <div><HR size="1px" color="#4F4F4F" style="margin-top:1.5vh; margin-bottom:2vh;"></div>
                  <div id="content">
                    <?php $research_content_3 = get_field( "research_content_3" );
                      if( $research_content_3 ){
                      echo $research_content_3;
                      }
                    ?>
                  </div>  
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6" >
              <div class="direction" style="width: 25vh;margin-bottom: 5vh;vertical-align: top;display: inline-block;">
                <div class="circle_block circle_right" style="width: 25vh; height: 25vh; ">
                  <?php
                  $image = get_field('reserch_direction_image_4');
                  if( !empty($image) ): ?>
                    <img id="circle" class="img-fluid" style="border-radius: 12.5vh; box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.4);" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                  <?php endif; ?>
                </div>
                <div class="title_left">
                  <div class="title_1">
                    <p>
                      <?php $research_title4 = get_field( "research_title4" );
                        if( $research_title4 ){
                          echo $research_title4;
                        }
                      ?>
                    </p>
                  </div>
                  <div><HR size="1px" color="#4F4F4F" style="margin-top:1.5vh; margin-bottom:2vh;"></div>
                  <div id="content">
                    <?php $research_content_4 = get_field( "research_content_4" );
                      if( $research_content_4 ){
                        echo $research_content_4;
                      }
                    ?>
                  </div>
                </div>
              </div>  
            </div>
          </div>
      </div>

</div><!-- /.container -->
</body>
