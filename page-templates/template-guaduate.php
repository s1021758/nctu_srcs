<?php
/*
 * Template Name: graduate
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(0)").addClass('nav_active');
      $ (".sidebarmenu a:eq(6)").removeClass('a_show')
      $ (".sidebarmenu a:eq(6)").addClass('sidebarmenu_active')
});

  </script>
  <script type="text/javascript">
    
  </script>
  <style>
    .title_block{
      display: none;
    }
  /*字體安排參考行政人員*/
    @media(max-width: 1024px){
      body{
        position: absolute;
        top: 0;
      }
      .main{
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      .title_block{
        display: flex;
      }
      .title_container{
        display: flex;
        width: 80vw !important;
      }
      .photo-graduate{
        display: block !important;
        margin-right: 0 !important;
      }
      .graduate-data{
        width :80vw !important;
        display: flex !important;
        flex-direction: column;
        align-items: center;
      }
      .list_div{
        display: none !important;
      }
      .border{
        border-right: 0 !important;
        width: auto !important;
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      #g2{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 300 !important;
        font-size: 1em !important;
        line-height: 1.5vh !important;
        letter-spacing: 0.12em !important;
        color: rgba(50, 50, 50, 1) !important;
      }
      #g1{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 600 !important;
        font-size: 1.4em !important;
        letter-spacing: 0.3em !important;
        color: rgba(50, 50, 50, 1) !important;
      }
      .graduate-name{
       width: auto;
       display: inline-block;
      }
      .ch-name{
        display: inline-block !important;
      }
      .eg-name{
        display: inline-block !important;
      }
      .education{
        display: inline-block;
        width: auto;
        height: auto !important;
        margin-bottom: 0vh !important;
      }
      .education p{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 1.8em !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        height: auto !important;
        margin-bottom: 2.5vh !important;
      }
      #graduate_email{
        font-size:0.5em !important;
      }
      .description{
        display: inline-block;
        margin-bottom: 2.5vh !important;
      }
      .experience{
        display: inline-block;
        width: auto;
      }
      .experience p{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 1.8em !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
      }
      .mobile_line{
        top:0vh;
      }

    }
  </style>
</head>
<?php get_template_part('includes/phone-list'); ?>
<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar'); ?>


</head>

<body>


<div class="container">
  <div class="row">

    <div class="main">
      <div class="title_block">
        <div class="title_container">
          <div class="title_r" style="width:10vw; float:left;">畢業生</div>
          <div class="mobile_title_lines"></div>
            <div class="botton_container2">
              <a href="<?php echo site_url(); ?>/current-student/"> 
                <img class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
              </a>
              <img onclick="show_menu()" class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
            </div>
          </div>
      </div>
      <div class="photo-graduate">
        <?php 

        $image = get_field('graduate_photo');

        if( !empty($image) ): ?>

          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

        <?php endif; ?>
      </div>
      <div class="graduate-data" style="width:50vw; height:auto; display:inline-block; vertical-align: top;">
        <div class="list_div" style="float: right; margin-top:5vh; margin-bottom: 3vh; display: inline-block;">
          <a href="<?php echo site_url(); ?>/graduate-list" class="back-to-list">回列表</a>
        </div>

        <div class="border" style="width: 52.5vw;  padding-right: 1vw;  border-right-color: rgba(60, 0, 110, 0.8);  border-right-style: solid;  border-right-width: 0.5vw;">
          <div class="graduate-name" style="height:4vh; margin-top:2vh;">

            <span class="ch-name" style="padding-right: 1.7vw; vertical-align: top;display: inline-block;float: left;border-right-style: solid; border-width: 2px;">
              <p id="g1">
                <?php $graduate_name_chi = get_field( "graduate_name_chi" );
                  if( $graduate_name_chi ){
                    echo $graduate_name_chi;
                  }
                ?>
              </p>
            </span>

            <span class="eg-name" style="padding-left: 1.7vw;vertical-align: top;display: inline-block; ">
              <p id="g1">
                <p id="g1">
                <?php $graduate_name_eng = get_field( "graduate_name_eng" );
                  if( $graduate_name_eng ){
                    echo $graduate_name_eng;
                  }
                ?>
              </p>
              </p>
            </span>
          </div>
          <!--畢業生職業-->
          <div class="graduate-name" style="height:2.5vh; margin-top:1vh; margin-bottom:2.5vh;">
            <span style="padding-right: 1.2vw; vertical-align: top;display: inline-block;float: left;border-right-style: solid; border-width: 2px;">
              <p id="g2">
                <?php $graduate_career_chi = get_field( "graduate_career_chi" );
                  if( $graduate_career_chi ){
                    echo $graduate_career_chi;
                  }
                ?>
              </p>
            </span>

            <span style="padding-left: 1.2vw;vertical-align: top;display: inline-block; ">
              <p id="g2">
                <?php $graduate_career_eng = get_field( "graduate_career_eng" );
                  if( $graduate_career_eng ){
                    echo $graduate_career_eng;
                  }
                ?>
              </p>
            </span>
          </div>

          <div class="description" style="font-weight: 500;font-size: 1.5em;line-height: 3vh;  letter-spacing: 0.1em;  color: rgba(50, 50, 50, 1); margin-bottom:5vh;">
            <p>
              <?php $motto = get_field( "motto" );
                if( $motto ){
                  echo $motto;
                }
              ?>
            </p>
          </div>
      </div>
        <div class="education" style="height:5vh; margin-bottom:3.25vh;">
          <p style="margin-bottom:0.75vh;">
            <?php $graduate_department = get_field( "graduate_department" );
              if( $graduate_department ){
                echo $graduate_department;
              }
            ?>
          </p>
        </div>

        <div class="education" style="height:4vh; margin-bottom:5vh;">
          <p id="graduate_email" style="margin-bottom:0.75vh;">
            <?php $graduate_info = get_field( "graduate_info" );
              if( $graduate_info ){
                echo $graduate_info;
              }
            ?>
          </p>
        </div>

        <div class="experience" style="width:47.75vw; height:auto; ">
            <p>
              <?php $graduate_experience = get_field( "graduate_experience" );
              if( $graduate_experience ){
                echo $graduate_experience;
              }
            ?>
            </p>
            

        </div>
      </div>


    </div>


  </div><!-- /.row -->
</div><!-- /.container -->
</body>
