<?php
/*
* Template Name: overseas
*/

?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/student.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(3)").addClass('nav_active');
      $ (".sidebarmenu a:eq(3)").removeClass('a_show')
      $ (".sidebarmenu a:eq(3)").addClass('sidebarmenu_active')
});

  </script>
  <style type="text/css">
    @media(max-width: 1024px){
      body{
        background-image: url("../wp-content/themes/nctu_srcs/images/cellphone_background.jpg");
        background-size: 100%;
      }
      .sidebarmenu{
        display: none;
      }
      .header{
        display: none;
      }
      .master-title{
        display: none;
      }
      .main{
        margin-top: 0vh !important;
        width: 80vw !important;
        margin-right: 10vw !important;
        margin-left: 10vw !important;
        height: 100vh;
      }
      .title_r{
        float: left;
        width: 24.5vw !important; 
      }
      .title_block{
        margin-top: 11vh;
        padding-bottom: 0vh;
      }
      .mobile_title_lines{
        width: 30vw;
      }
      .background_image img{
        display: none;
      }
      .overseas-text{
        background-color: rgba(0,0,0,0);
        padding-left: 0vw;
        padding-top: 0vh;
        z-index: 9999;
        height: 29.375vh;
        margin-top: 0vh;
        position: relative;
      }
      .overseas-title{
        display: none;
      }

      .overseas-subtitle{
        margin-top: 1.25vh;
        font-weight: 300;
        font-size: 0.8em;
        line-height: 1.75vh;
        letter-spacing: 0.1em;
        color: rgba(0, 0, 0, 1);
      }
      .overseas-content{
        width: 80vw;
        font-weight: 300;
        font-size: 0.8em;
        line-height: 1.8em;
        letter-spacing: 0.1em;
        color: rgba(0,0,0, 1);
        text-align: justify;
      }
      .overseas-text hr{
        display: none;
      }
      .btn{
        border: 1px solid #000;
        width: 11vw;
        height: 11vw;        
      }
      .btn-content span{
        color: rgba(0,0,0, 1) !important;
      }
    }
    @media(min-width: 1025px){
      .title_block{
        display: none;
      }
      .btn{
        border: 1px solid #fff;
        margin-right: 3vw;
      }
      .clear_both{
        display: none;
      }
    }
    @media(max-width: 512px){
      .btn-content{
        font-size: 0.5em;
      }
    }
  </style>      
</head>

<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar-student-recruitment'); ?>
<?php get_template_part('includes/phone-list'); ?>

    <div class="main" style="width:76vw;">
<!--phone title-->
          <div class="title_block">
            <div class="title_r">僑生港澳生招生</div>
            <div class="mobile_title_lines"></div>
            <div class="botton_container">
              <a href="<?php echo site_url(); ?>/doctor/"> 
                <img class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
              </a>
              <img onclick="show_menu()" class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
              <a href="<?php echo site_url(); ?>/chinese/"> 
                <img class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_right_dark_grey.svg">
              </a>
            </div>
          </div> 
          <div class="clear_both"></div>
<!--phone title end-->      
      <div class="background_image">
        <img style="z-index:-1;" src="<?php bloginfo('template_url'); ?>/images/admissions_img_03.jpg" />
        <div class="overseas-text" >
          <a target="_blank" href="https://aadm.nctu.edu.tw/admit/" class="btn btn-content">
            <span style="color: #fff;">交大
              綜合組</span>
          </a>
          <div class="overseas-title">僑生港澳生招生</div>
          <div class="overseas-subtitle">
            <?php $overseas_subtitle = get_field( "overseas_subtitle" );
            if( $overseas_subtitle ){
              echo $overseas_subtitle;
            }
            ?>
          </div>
          <HR size="1px" style="margin-top:2.75vh; margin-bottom:1.5vh; width:62.5vw; color:#fff;">
          <div class="clear_both"></div>
          <div class="overseas-content">
            <?php $overseas_content = get_field( "overseas_content" );
            if( $overseas_content ){
              echo $overseas_content;
            }
            ?>
          </div>

        </div>
      </div>
      
    </div>
