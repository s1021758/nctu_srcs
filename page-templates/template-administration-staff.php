<?php
/*
 * Template Name: administration-staff
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(0)").addClass('nav_active');
      $ (".sidebarmenu a:eq(4)").removeClass('a_show')
      $ (".sidebarmenu a:eq(4)").addClass('sidebarmenu_active')
});

  </script>
  <style>
    .mobile_img{
      display: none
    }
    .mobile_staff_line{
      display: none;
    }
    .professor_block_line{
      display: none;
    }
    .professor_color_block{
      display: none;
    }
    .img{
      width: auto;
    }
    @media(min-width: 768px){
      .professor_block_text3{
        left:-52vw !important;
      }
    }
    @media(max-width: 1024px){
      body{
        position: absolute;
        top: 0;
        left: 0;
      }
      #hr-line{
        display: none;
      }
      .main_long{
        display: none;
      }
      .professor_big_image{
        width: 97vw !important;
      }
      .img{
        width: 0px;
        visibility: hidden;
        background-image:url("../wp-content/themes/nctu_srcs/images/professor/prof劉紀蕙_photo3.jpg"); 
      }
      /*圖片大小不確定*/
      .mobile_img{
        display: inline-block;
        width: auto;
        height: auto;
        max-width: 48.5vw;
        max-height: 32.5vh;
      }
      .professor_text{
        display: inline-block;
      }
      .professor_block{
        padding-bottom: 13.25vh !important;
        position: relative;
        bottom:16.25vh;
        padding-right: 0 !important;
        border-right: 1px solid #000;
        border-bottom: 1px solid #000;
      }
      .professor_color_block{
        display: block;
        width: 7vw;
        height: 1vh;
        background-color: rgba(60, 0, 110, 0.8);
        position:absolute;
        top:0vh;
        right:3vw;
      }
      .professor_block_line{
        display: inline;
        flex-grow: 1;
        width: auto;
        border-top: 1px solid #000;
      }
      .professor_block_text1{
        display: inline-flex;
        position: relative;
        bottom:0.7vh;
        width:33vw !important;
      }
      .professor_block_text1 p{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 300 !important;
        font-size: 1em !important;
        line-height: 1.5vh !important;
        letter-spacing: 0.12em !important;
        color: rgba(50, 50, 50, 1) !important;
        width: auto;
        position: relative;
        bottom: 0.55vh;
      }
      .professor_block_text2 p{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 600 !important;
        font-size: 1.8em !important;
        letter-spacing: 0.3em !important;
        color: rgba(50, 50, 50, 1) !important;
      }
      .professor_block_text3{
        position: absolute;
        top:28vh;
        left:-52.5vw;
        width: auto !important;
      }
      .professor_block_text3  p{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 1.8em !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        border-top: 0 !important;
      }
      .inblock{
        margin-top: 15.5vh !important;
      }
      .mobile_line_top{
        display: block;
        height: 1px;
        border: none;
        border-top:1px solid #000;
        position: relative;
        top:-2.5vh;
        width: 15px;
      }
      .mobile_line{
        top:0;
        padding-top: 2vh;
      }
      .staff{
        display: none;
      }
      .staff_name{
        display: inline-flex !important;
        width: 80vw !important;
        align-items: center;
      }
      .staff_name #n1{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 300 !important;
        font-size: 1em !important;
        line-height: 1.5vh !important;
        letter-spacing: 0.12em !important;
        color: rgba(50, 50, 50, 1) !important;
        width: auto;
        display: inline;
        order: 2;
      }
      .staff_name #n2{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 1.8em !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        width: auto;
        display: inline;
        order: 1;
        margin-right: 2vw;
      }
      .mobile_staff_line{
        display: inline;
        order:3;
        width:auto;
        flex-grow: 1;
        height:1px;
        border-top: 1px solid #000;
        position: relative;
        top:50%;
        transform:translateY(-50%);
      }
      .staff_data{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 1.8em !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        width:80vw !important;
      }
      .job_content{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 1.8em !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        width: 80vw !important;
      }
      .line{
        display: none;
      }
      .botton_container{
        position: static;
        float: right;
      }
      .menu-btn{
        margin-top: 0vh !important;
        margin-bottom:4vh;
      }
    }
    @media(min-width: 1025px){
      .title_block{
        display: none;
      }
    }
  </style>
</head>
<?php get_template_part('includes/phone-list'); ?>
<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar'); ?>

<div class="container"> 
  <div class="row">
    <div class="main" > 
      <div class="title_block">
        <div class="title_r">行政人員</div>
        <div class="mobile_title_lines"></div>
        <div class="botton_container">
            <a href="<?php echo site_url(); ?>/guest-professor/">
              <img class="botton1"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
            </a> 
            <img  onclick="show_menu(),close_newpost()" class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
            <a href="<?php echo site_url(); ?>/current-student/">
              <img class="botton2"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_right_dark_grey.svg">
            </a>  
          </div>
      </div>
      <div class="main_long"><font>行政人員</font></div>
      <div class="professor_big_image">
        <?php 

        $image = get_field('chairman_photo');

        if( !empty($image) ): ?>
          
          <img class="img" style="width=:71.875vw; z-index:-1;" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

        <?php endif; ?>
        <!-- mobile picture -->
        <img class="mobile_img"src="../wp-content/themes/nctu_srcs/images/professor/prof劉紀蕙_photo3.jpg">
        <!-- <img style="width=:71.875vw; z-index:-1;" src="<?php bloginfo('template_url'); ?>/images/graduate_chair.jpg" /> -->
        <div class="professor_text" style="width:34.5vw; margin-top:-27vh; margin-left:2.5vw;">
          <div class="professor_block" style="padding: 1vw 1vh;">
            <div class="professor_block_text1" style="width:32vw; margin-bottom:1vh;">
              <p>社文所&nbsp;所長</p>
              <div class="professor_block_line"></div>
            </div>
            <div class="professor_color_block"></div>
            <div class="professor_block_text2" style="width:32vw; height:4.75vh; margin-bottom:2.25vh;">
              <p>
                <?php $chairman_name = get_field( "chairman_name" );
                  if( $chairman_name ){
                    echo $chairman_name;
                  }
                ?>
            </p>
              <!-- <p>劉紀慧</p> -->
            </div>
            <div id="hr-line" style="margin-top:4.25vh; margin-bottom:1.5vh;">
              <HR style="color:rgba(255,255,255,0.8);" size="2px" >
            </div>

            <div class="professor_block_text3" style="width:32vw;">
              <hr class="mobile_line" />
              <p>
                <?php $chairman_education = get_field( "chairman_education" );
                  if( $chairman_education ){
                    echo $chairman_education;
                  }
                ?>
              </p>
              <!-- <p>美國伊利諾大學比較文學博士</p> -->
            </div>
          </div>
        </div>

      </div>
    <div class="staff_block" style="top:5vh;">
        <div class="staff" style="width: 73.625vw; position: relative;">
          <HR color="#323232" size="4px" style="margin-bottom:2px;" >
          <HR color="#323232" size="1px" >
        </div>
        <!--行政人員欄位-->

            <?php $administration_staff = get_post_meta( $post->ID, 'administration_staff', true );
              foreach( $administration_staff as $staff){?>
              <div class="inblock" style="width:73.875vw; margin-top: 3vh;  height: auto;">
                <hr class="mobile_line_top" />
                <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;display: inline-block; vertical-align: top; height: auto;">
                  <div id="n1">行政人員</div>
                  <div id="n2"><?php echo $staff['staff_name']?></div>
                  <div class="mobile_staff_line"></div>
                </div>
                <hr class="mobile_line" />
                <div class="staff_data" style="width:14vw; display: inline-block;vertical-align: top; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">
                  <p><?php echo $staff['staff_contact']?></p>
                  
                </div>
                <hr class="mobile_line" />
                <div class="job_content" style="width:49vw; height: auto;    display: inline-block;">
                  <p><?php echo $staff['staff_job_content']?></p>
                  <!-- <HR size="1px" color="#4F4F4F" style="width:21vw; margin-left: -25vw;"> -->
                </div>
                <div class="line" style="width:21vw; margin-bottom: 5vh;">
                  <HR size="1px" color="#4F4F4F">
                </div>
              </div>
              <?php

            }?>


        <!-- <div class="inblock" style="width:73.875vw; margin-top: 3vh;">
            <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;float: left;">
              <div id="n1">行政人員</div>
              <div id="n2">洪慧芳</div>
            </div>
            <span class="staff_data" style="width:14vw; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">
              <li>03-5131593#31593</li>
              <li>03-5734450</li>
              <li>hungfa@mail.nctu.edu.tw</li>
            </span>
            <span class="job_content" style="width:49vw; height:32vh; float:right;">
              <ol>
                <li>學生入學、畢業作業、選課事宜、受理獎、助學金之申請及造冊、導師費印領清冊。</li>
                <li>期刊、圖書、設備採購及維修，學校經費、教學精進計畫經費核銷、公文收發、簽辦。</li>
                <li>專、兼任教師公開徵聘作業、新聘報到、升等、評鑑、授課鐘點、交通費核算申報，宿舍及外國人工作證申請。</li>
                <li>課程資料搜集、編排、修課規定修訂、大綱上傳及必修課成績上傳。</li>
                <li>本所組織章程、定位、所教評會組織、升等辦法、獎助學金辦法及施行細則
                <li>招生試務工作小組組成辦法及考試資格審查辦法訂定與修訂。</li>
                <li>系所自我評鑑作業及大專校院系所評鑑作業。</li>
                <li>碩（碩甄試、一般入學考試及陸生、僑生、外籍生入學審查）</li>
                <li>博班（一般入學考試及僑生、外籍生入學審查）招生與外籍生獎學金審查工作。</li>
                <li>所教評會、所務會議聯絡、提案資料準備及簽辦。</li>
                <li>教師彈性薪資教學及研發資料系統維護申報，教育部建置全國教育資訊管理服務</li>
                <li>總量管制系統及台經院問卷之資料庫填寫。</li>
                <li>本所空間及財產保管與報廢作業。</li>
                <li>國科會專題研究計畫線上彙整、變更作業。</li>
                <li>所長交辦事宜。</li>
              </ol>
            </span>
            <div class="line" style="margin-top:51.75vh; width:21vw;">
              <HR size="1px" color="#4F4F4F">
            </div>
        </div>

        <div class="inblock" style="width:73.875vw; margin-top: 3vh;">
            <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;float: left;">
              <div id="n1">行政人員</div>
              <div id="n2">林郁曄</div>
            </div>
            <span class="staff_data" style="width:14vw; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">
              <li>03-5712121#31521</li>
              <li>03-5734450</li>
              <li>leaves.taiwan@gmail.com</li>
            </span>
            <span class="job_content" style="width:49vw; float:right;">
              <ol>
                <li>研究群整合計劃（人社中心、研發推動計畫等）活動規劃、執行與經費核銷</li>
                <li>台灣聯大文化研究跨校學程之推展與執行，國際學程計畫規劃與交流</li>
                <li>國際學者交流訪問計劃申請、執行與經費核銷</li>
                <li>國內外學術演講及國內外學術研討會議執行與經費核銷</li>
                <li>新興文化研究中心網頁維護及資料庫建置</li>
              </ol>
            </span>
            <div class="line" style="margin-top:8.75vh; width:21vw;">
              <HR size="1px" color="#4F4F4F">
            </div>
        </div>

        <div class="inblock" style="width:73.875vw; margin-top: 3vh;">
            <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;float: left;">
              <div id="n1">行政人員</div>
              <div id="n2">曾美儀</div>
            </div>
            <span class="staff_data" style="width:14vw; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">
              <li>03-5731657</li>
              <li>iics.mavis@g2.nctu.edu.tw</li>
            </span>
            <span class="job_content" style="width:49vw; float:right;  ">
              <ol>
                <li>綜理台聯大文化研究國際中心外籍學生</li>
                <li>訪問學者與國際交流相關事宜</li>
                <li>協助中心辦理研討會與學術活動相關事宜</li>
                <li>協助推動學位學程國際化相關業務</li>
                <li>協助推動跨校學位學程</li>
              </ol>
            </span>
            <div class="line" style="margin-top:12.75vh; width:21vw;">
              <HR size="1px" color="#4F4F4F">
            </div>
        </div>


        <div class="inblock" style="width:73.875vw; margin-top: 3vh;">
            <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;float: left;">
              <div id="n1">行政人員</div>
              <div id="n2">彭啟禎</div>
            </div>
            <span class="staff_data" style="width:14vw; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">

              <li>03-5731657</li>
              <li>hubertp@ms53.hinet.net</li>
            </span>
            <span class="job_content" style="width:49vw; float:right;  list-style-type: none;">
                <li>網頁視覺設計</li>
            </span>
            <div class="line" style="margin-top:8.75vh; width:21vw;">
              <HR size="1px" color="#4F4F4F">
            </div>
        </div>


        <div class="inblock" style="width:73.875vw; margin-top: 3vh;">
            <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;float: left;">
              <div id="n1">行政人員</div>
              <div id="n2">許智鈞</div>
            </div>
            <span class="staff_data" style="width:14vw; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">

              <li>0986-656-299</li>
              <li>yoyotvyoo@gmail.com</li>
            </span>
            <span class="job_content" style="width:49vw; float:right; list-style-type: none;">
              <li>網管</li>
            </span>
            <div class="line" style="margin-top:2.75vh; width:21vw;">
              <HR size="1px" color="#4F4F4F">
            </div>
        </div>

        <div class="inblock" style="width:73.875vw; margin-top: 3vh;">
            <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;float: left;">
              <div id="n1">行政人員</div>
              <div id="n2">蘇凰蘭</div>
              <div id="n1">博士後研究員</div>
            </div>
            <span class="staff_data" style="width:14vw; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">

              <li>03-5731658</li>
              <li>iics.center@gmail.com</li>
            </span>
            <span class="job_content" style="width:49vw; float:right;">
              <ol>
                <li>綜理台聯大文化研究國際中心行政業務中心</li>
                <li>中心國際化業務</li>
                <li>中心預算編列</li>
                <li>中心通訊編輯</li>
                <li>中心例行性會議籌備與執行</li>
                <li>籌劃中心舉辦研討會與學術活動相關事宜</li>
                <li>協助推動跨校學位學程</li>

              </ol>
            </span>
            <div class="line" style="margin-top:21.75vh; width:21vw;">
              <HR size="1px" color="#4F4F4F">
            </div>
        </div>

        <div class="inblock" style="width:73.875vw; margin-top: 3vh;">
            <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;float: left;">
              <div id="n1">行政人員</div>
              <div id="n2">蘇淑芬</div>
            </div>
            <span class="staff_data" style="width:14vw; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">

              <li>03-5712121#58259</li>
              <li>apcsapcs@gmail.com</li>
            </span>
            <span class="job_content" style="width:49vw; float:right;">
              <ol>
                <li>國內外學術演講及學者訪問活動安排</li>
                <li>交通大學亞太/文化研究室統籌業務</li>
              </ol>
            </span>
            <div class="line" style="margin-top:2.75vh; width:21vw;">
              <HR size="1px" color="#4F4F4F">
            </div>
        </div>


        <div class="inblock" style="width:73.875vw; margin-top: 3vh;">
            <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;float: left;">
              <div id="n1">行政人員</div>
              <div id="n2">林家瑄</div>
            </div>
            <span class="staff_data" style="width:14vw; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">

              <li>03-5712121#58259</li>
              <li>iacsiacsiacs@gmail.com</li>
            </span>
            <span class="job_content" style="width:49vw;float:right;">
              <ol>

                <li>負責Inter-Asia Cultural Studies國際性學術期刊之編輯業務</li>
                <li>執行亞太/文化研究室相關計畫</li>
              </ol>
            </span>
            <div class="line" style="margin-top:2.75vh; width:21vw;">
              <HR size="1px" color="#4F4F4F">
            </div>
        </div>


        <div class="inblock" style="width:73.875vw; margin-top: 3vh;">
            <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;float: left;">
              <div id="n1">行政人員</div>
              <div id="n2">林麗雲</div>
            </div>
            <span class="staff_data" style="width:14vw; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">

              <li>03-5712121#58259</li>
              <li>taiwanliyun@gmail.com</li>
            </span>
            <span class="job_content" style="width:49vw; float:right;">
              <ol>
                <li>執行尋畫－由畫家吳耀忠出發的左翼拼圖計畫報導文學創作</li>
                <li>收集尋畫－由畫家吳耀忠出發的左翼拼圖計畫相關資料</li>
                <li>聯絡藏畫所有者並進行深度採訪</li>
              </ol>
            </span>
            <div class="line" style="margin-top:2.75vh; width:21vw;">
              <HR size="1px" color="#4F4F4F">
            </div>
        </div>


        <div class="inblock" style="width:73.875vw; margin-top: 3vh;">
            <div class="staff_name" style="width:8.28125vw; margin-right:0.78125vw;float: left;">
              <div id="n1">行政人員</div>
              <div id="n2">黃勤雯</div>
            </div>
            <span class="staff_data" style="width:14vw; margin-right: 0.78125vw; list-style-type: none; display: inline-block;">

              <li>03-5712121#58273</li>
              <li>chinwen555@gmail.com</li>
            </span>
            <span class="job_content" style="width:49vw; float:right; list-style-type: none;">
                <li>執行教育部優秀外國青年來臺短期蹲點計畫─亞際文化研究全球網絡計畫書</li>

            </span>
            <div class="line" style="margin-top:2.75vh; width:21vw;">
              <HR size="1px" color="#4F4F4F">
            </div>
        </div> -->




      </div>




  </div><!-- /.row -->
</div><!-- /.container -->



</?php get_template_part('includes/footer'); ?>
