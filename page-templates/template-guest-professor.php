<?php
/*
 * Template Name: guest-professor
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(0)").addClass('nav_active');
      $ (".sidebarmenu a:eq(3)").removeClass('a_show')
      $ (".sidebarmenu a:eq(3)").addClass('sidebarmenu_active')
});

  </script>
  <style type="text/css">
    .teacher_mobile_line{
      display: none;
    }
    @media(min-width: 1025px){
      .title_block{
        display: none;
      }
    }
    /*同專任師資*/
    @media(max-width: 1024px){
      .main_long{
        display: none;
      }
      .name_block{
        flex-direction: column;
        width: 80vw !important;
        margin-top: 0 !important;
      }
      .name_block .name_block{
        margin-top: 1.5vh !important;
        position: relative;
        top : -1.25vh;
      }
      .name_block span{
        width: 80vw !important;
      }
      .guest_professor_name{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 500 !important;
        font-size: 1.25em !important;
        line-height: 2vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        display: flex;
      }
      .teacher_mobile_line{
        display: inline-block !important;
        width: auto;
        margin-left: 1vw;
        flex-grow: 1;
        order: 3;
        border-top:1px solid #000;
        position: relative;
        top:0.9vh;
      }
      .guest_professor_type{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 2vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        margin-top: 1vh;
      }
      .guest_professor_education{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 0.8em !important;
        line-height: 1.5vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        margin-top: 1vh;
      }
      .guest_professor_expertise{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 300 !important;
        font-size: 0.7em !important;
        line-height: 1.3em !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        margin-top: 1vh;
      }
    }


    
  </style>
</head>
<?php get_template_part('includes/phone-list'); ?>
<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar'); ?>


<div class="container">
  <div class="row">
    <div class="main" >
      <div class="main_long"><font>客座教授</font></div>
      <!--phone title-->
      <div class="title_block">
        <div class="title_r">客座教授</div>
        <div class="mobile_title_lines"></div>
        <div class="botton_container">
            <a href="<?php echo site_url(); ?>/adjunct-teacher/">
              <img class="botton1"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
            </a> 
            <img  onclick="show_menu(),close_newpost()" class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
            <a href="<?php echo site_url(); ?>/administration-staff/">
              <img class="botton2"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_right_dark_grey.svg">
            </a>  
          </div>
      </div>

      <div class="name_block" style="width: 71.875vw; margin-top: 4vh; margin-bottom: 1.25vh;">
        <!-- becareful the name of the post id  "guest_profeesor" which having the double e -->
        <?php $guest_professor = get_post_meta( $post->ID, 'guest_profeesor', true );
            foreach( $guest_professor as $guest_pro){?>
              <div class="name_block" style="width:71.875vw; display: inline-flex; margin-top: 1.25vh; margin-bottom: 1.25vh;">
                <span class="guest_professor_name" style="width:10.5vw; margin-right:0.5vw; letter-spacing: 0.3px;"><?php echo $guest_pro['guest_profeesor_name']?>
                  <div class="teacher_mobile_line"></div>
                </span>
                <span class="guest_professor_type" style="width:13.125vw; margin-right:0.5vw;"><?php echo $guest_pro['guest_profeesor_type']?></span>
                <span class="guest_professor_education" style="width:24.5vw; margin-right:0.5vw;"><?php echo $guest_pro['guest_profeesor_education']?></span>
                <span class="guest_professor_expertise" style="width:31.875vw;"><?php echo $guest_pro['guest_profeesor_expertise']?></span>
              </div>
              <?php

            }?>
      </div>
    </div>

  </div><!-- /.row -->
</div><!-- /.container -->
