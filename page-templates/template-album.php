<?php
/*
 * Template Name: album
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/table.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/student.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(4)").addClass('nav_active');
      $ (".sidebarmenu a:eq(3)").removeClass('a_show');
      $ (".sidebarmenu a:eq(3)").addClass('sidebarmenu_active');
});

  </script>
  <!-- js for mobile -->
  <script>
    $(document).ready(function(){
      if(screen.width<=1024){
        $(".semester").attr("disabled",true);
      }
    });
    function dropdown(){
      // 下拉選單安紐功能 
      var x = $(".semester").children("option").length;
      if(screen.width<=1024 && $(".semester").attr("size")==1){
        $(".semester").attr("disabled",false);
        $(".semester").attr("size",x);
        $(".semester").css("cssText","height:auto !important");
        $(".semester").append("<option value='' selected='selected'></option>");
        $(".semester").find(":selected").remove();
      }
      else if(screen.width<=1024 && $(".semester").attr("size")==x){
        $(".semester").attr("disabled",true);
        $(".semester").attr("size",1);
        $(".semester").css("cssText","height:3.125vh !important");
      }
      $(".semester").change(function(){
        if(screen.width<=1024){
          $(".semester").attr("disabled",true);
          $(".semester").attr("size",1);
          $(".semester").css("cssText","height:3.125vh !important");
        }
      });
    }
  </script>
  <style>
    .button{
      display: none;
    }
    @media(max-width: 1024px){
      .options{
        position: relative;
        display: flex;
        float: none !important;
        width: auto !important;
        height: 3.125vh !important;
        margin-bottom: 3vh;
        margin-top: 2.5vh;
      }
      .options form{
        display: inline-block;
      }
      .options form select option{
        color: rgba(255, 255, 255, 1);
      }
      .semester{
        width: 16vw !important; 
        height: 3.125vh !important;
        border-radius: 3px !important;
        background: rgba(60, 0, 110, 0.8) !important;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1.2em !important;
        line-height: 3.125vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(255, 255, 255, 1) !important;
        margin-right: 2vw;
      }
      .button{
        display: inline-block;
        width:4vw;
        max-width: 24px;
      }
      .title_block{
        margin-bottom: 0vh !important;
        padding-bottom:0vh !important;
      }
      .album-block{
        width: 80vw !important;
        display: flex;
        flex-wrap: wrap;
      }
      .a-text{
        width: 21vw;
        height: auto !important;
      }
      #a1{
        width: 21vw;
        height: auto;
        word-break: normal;
      }
    }
    @media(min-width: 1025px){
      .title_block{
        display: none;
      }
    }
  </style>
</head>
<?php get_template_part('includes/phone-list'); ?>
<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar-global-lounge'); ?>


<body>

    <div class="main" style="margin-bottom:5vh;">
      <div class="title_block" style="width:73vw; height:5vh; margin-bottom: 3.75vh;">
        <div class="title_r" style="width:25vw; float:left;">活動相簿</div>
        <div class="mobile_title_lines"></div>
        <div class="botton_container2">
            <a href="<?php echo site_url(); ?>/workshop/"> 
              <img class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
            </a>
            <img onclick="show_menu()" class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
        </div>
      </div>
        <div class="options" style="width:48vw; float:right; ">
            <form>
            <select size="1" class="semester" style="background: #e6e6e6 url('<?php bloginfo('template_url'); ?>/images/btn/arrowhead-pointing-down.png')  no-repeat; background-position: 10vw;">
            　<option value="106_first_sem">2017</option>
            　<option value="105_second_sem">2016</option>
            　<option value="105_first_sem">2015</option>
            </select>
            </form>
            <img onclick="dropdown()" class="button"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_down_dark_grey.svg">
        </div>




      <div class="album-block" style="width:72vw;  margin-bottom: 5vh;">
<?php
// wp query
$args = array(
  'category_name' => 'gallery_srcs',
  'orderby' => 'title',
  'order'   => 'DESC',
);
$the_query = new WP_Query($args);
if($the_query->have_posts()):
    while($the_query->have_posts()):
        $the_query->the_post();
        $images = acf_photo_gallery('album_detail_gallery', $post->ID);
        $image=$images[0];
        $img_url=$image['full_image_url'];
?>
        <div class="a-single" style="margin-right:3vw;"> <!--單一個album-->
          <a href="<?php the_permalink();?>" style="text-decoration:none;">
            <div class="a-image" style="width:21vw; height:21vw;">
              <img src="<?php echo $img_url; ?>" />
            </div>
            <div class="a-text" style="height:10vh; background-color:rgba(230,230,230,1); padding:0 0.625vw">
              <div id="a1"><?php the_title();?></div>
              <div id="a2">
                <?php 
                  // $image = array();
                  if(!empty($images)){
                  echo count($images) ; 
                  }
                  else{
                    echo 0;
                  }
                ?>
                張照片
              </div>
              <div id="a3"><?php the_time('Y/m/d') ?></div>
            </div>
          </a>
        </div>
<?php endwhile; endif; ?>

      </div>




    </div>

    </div>


</body>








