<?php
/*
 * Template Name: current-student
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" /> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(0)").addClass('nav_active');
      $ (".sidebarmenu a:eq(5)").removeClass('a_show');
      $ (".sidebarmenu a:eq(5)").addClass('sidebarmenu_active');


      $('#btn_year').click(function(){
        
        if($('#1').css('display')!='none'){
          $('#2').html($('#static').html()).show();
          $('#1').hide();
          $ ("#btn_area").removeClass('button_style_black');
          $ ("#btn_area").addClass('button_style_white');
          $ ("#btn_year").removeClass('button_style_white');
          $ ("#btn_year").addClass('button_style_black'); 
        }
    });

      $('#btn_area').click(function(){
        
        if($('#2').css('display')!='none'){
          $('#1').show();
          $('#2').hide();
          $ ("#btn_year").removeClass('button_style_black');
          $ ("#btn_year").addClass('button_style_white');
          $ ("#btn_area").removeClass('button_style_white');
          $ ("#btn_area").addClass('button_style_black'); 
        }
    });


});


    
  </script>
  <style type="text/css">
    @media(max-width: 1024px){
      body{
        background-image: url("../wp-content/themes/nctu_srcs/images/cellphone_background.jpg");
        background-size: 100%;
        position: absolute;
        top: 0;
      }
      .sidebarmenu{
        display: none;
      }
      .header{
        display: none;
      }
      .master-title{
        display: none;
      }
      .main{
        margin-top: 0vh !important;

      }
      .title_r{
        float: left;
      }
      .title_block{
        margin-top: 11vh;
        padding-bottom: 0vh;
        margin-bottom: 2.5vh;
      }
      .main_short{
        display: none;
      }
      .m{
        width: 80vw;
      }
      .block_content{
        width: 80vw;
      }
      .main_block{
        width: 39vw; 
        display:inline-block; 
        margin-right:0; 
        vertical-align: top;
      }
      .names {
        margin-top: 0.5vh;
        height: auto;
        width: 39vw !important;
        text-align:center;
      }
      .category{
        text-align: center;
      }
    }
    @media(min-width: 1025px){
      .title_block{
        display: none;
      }
      .m{
        width:33vw; 
        display: inline-block;
      }
      .main_block{
        width: 34.6vw; 
        display:inline-block; 
        margin-right: 2.5vw; 
        vertical-align: top;
      }

    }

  </style>
</head>

<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar'); ?>
<?php get_template_part('includes/phone-list'); ?>

<div class="container">
  <div class="row">
    <div class="main">

<!--phone title-->
      <div class="title_block">
        <div class="title_r">在校生</div>
        <div class="mobile_title_lines"></div>
        <div class="botton_container">
          <a href="<?php echo site_url(); ?>/administration-staff/">
            <img class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
          </a>
          <img onclick="show_menu()" class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
          <a href="<?php echo site_url(); ?>/graduate-list/">
            <img class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_right_dark_grey.svg">
          </a>
        </div>
      </div> 
      <div class="clear_both"></div>
<!--phone title end-->   

      <div class="m">
        <div class="main_short"><font>在校生</font></div>
      </div>

      <div name="wip">
        <br>
        <big>頁面維護中，請先參考本站其他頁面</big>
        <br>
        <img src="../wp-content/themes/nctu_srcs/images/wip.svg" style="max-width: 300px;">
      </div>

    </div>
  </div><!-- /.row -->
</div><!-- /.container -->



</?php get_template_part('includes/footer'); ?>
