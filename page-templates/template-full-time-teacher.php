<?php
/*
 * Template Name: full-time-teacher
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(0)").addClass('nav_active');
      $ (".sidebarmenu a:eq(1)").removeClass('a_show');
      $ (".sidebarmenu a:eq(1)").addClass('sidebarmenu_active');
});
    /*cancel the link of "a" tag in mobile*/
   function cancel_link(obj){
    if(screen.width <= 1024){
    obj.href = "javascript:void(0)";
      }
   }

  </script>
  <style type="text/css">
    .teacher_mobile_line{
      display: none;
    }
    .mobile_home_button{
      display: none;
    }
    .inblock{
      height: auto !important;
      margin-bottom: 17vh !important;
    }
    .teacher_img{
      height: 24vh;
    }
    .teacher_block{
      display: inline-block;
    }
    @media(max-width: 1025px){
      body{
        background-image: url("../wp-content/themes/nctu_srcs/images/cellphone_background.jpg");
        background-size: 100%;
      }
      .main_long{
        display: none;
      }
      .line{
        display: none;
      }
      .photo{
        display: none;
      }
      #t1.t1{
        display:inline-block;
        width: auto !important;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 2vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        margin-left: 1vw;
        order: 2;
      }
      #t1.t2{
        display: inline-block;
        width: auto !important;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 500 !important;
        font-size: 1.25em !important;
        line-height: 2vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        order: 1;
        margin-left: 0 !important;
      }
      #t1{
        display:inline-block;
        width: auto !important;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 2vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        margin-left: 1vw;
        order: 2;
      }
      #t2{
        display: inline-block;
        width: auto !important;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 500 !important;
        font-size: 1.25em !important;
        line-height: 2vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        order: 1;
      }
      .teacher_mobile_line{
        width: auto;
        margin-left: 1vw;
        display: inline-flex;
        flex-grow: 1;
        order: 3;
        border-top:1px solid #000;
        position: relative;
        top:0.9vh;
      }
      .teacher{
        display: flex !important;
        width: 80vw !important;
        margin-bottom: 1vh;
      }
      .teacher_block{
        display: flex !important;
        flex-direction: column;
        left: 10vw;
        width: 80vw !important;
        margin-right: 0;
        margin-left: 0 !important;
        margin-top: 0 !important;
        height: auto !important;
      }
      #graduate{
        display: flex;
        justify-content: space-between;
        height: 1.5vh;
        width: 80vw;
        float: none;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 0.8em !important;
        line-height: 1.5vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
        margin-bottom: 1vh !important;
      }
      .profession{
        width:auto !important; 
        height: auto !important;
      }
      .profession hr{
        display: none;
      }
      .profession ol.v{
        height: auto;
      }
      .mobile_top_content_container{
        height: 7.5vh !important;
        margin-bottom: 2.5vh !important;
      }
      .mobile_home_button{
        display: inline-block;
        width: 5.5vw;
        max-width: 34px;
        height: 5.5vw;
        max-height: 34px;
      }
      .v{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 300 !important;
        font-size: 0.7em !important;
        line-height: 1.3em !important;
        letter-spacing: 0.1em !important;
        width: 70vw;
      }
      #t2.mobile_top_content{
        display: flex;
        width: 70vw !important;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 300 !important;
        font-size: 0.7em !important;
        line-height: 1.3em !important;
        letter-spacing: 0.1em !important;
      }
      #t2 br{
        display: none !important;
      }
      .inblock{
        width: 80vw !important;
        left: 10vw !important;
        display: flex;
        flex-direction: column;
        margin-bottom: 0 !important;
      }
    }
    @media(min-width: 1025px){
      .title_block{
        display: none;
      }
    }
  </style>
</head>
<?php get_template_part('includes/phone-list'); ?>
<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar'); ?>



<div class="container">
  <div class="row">
    <div class="main" >
      <div class="title_block">
        <div class="title_r">專任師資</div>
        <div class="mobile_title_lines"></div>
        <div class="botton_container">
          <a href="<?php echo site_url(); ?>/aboutus/"> 
            <img class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
          </a>
            <img onclick="show_menu()" class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
          <a href="<?php echo site_url(); ?>/adjunct-teacher/"> 
            <img class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_right_dark_grey.svg">
          </a>
        </div>
      </div> 
      <div class="clear_both"></div>
      <div class="main_long"><font>專任師資</font></div>
          <?php
          $is_multiple=False;
          $args = array(
          'category_name' => 'fulltime_teacher_info_srcs',
          'orderby' => 'title',
          'order' => 'ASC',
          'posts_per_page' => '100'
          );
          $the_query = new WP_Query($args);
          if($the_query->have_posts()):
              while($the_query->have_posts()):
                  $the_query->the_post();
                  if($is_multiple):
          ?>
          <?php endif; ?>
          <div class="teacher_block" style=" margin-right: 1vw; display: inline-block; margin-top: 5vh; height: auto;">
            <a target="_blank" href="<?php the_permalink(); ?>" onclick = "cancel_link(this)">
                <div class="photo">
                  <?php 

                  $image = get_field('teacher_photo');

                  if( !empty($image) ): ?>

                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

                  <?php endif; ?>
                </div>
                <div class="teacher">
                  <div id="t1" class="t1" style="margin-right: 1vw;">
                    <?php $teacher_position = get_field( "teacher_position" ); 
                      if( $teacher_position ){
                        echo $teacher_position;
                      }
                    ?>
                  </div>
                  <div id="t1" class="t2" style="margin-right: 1vw; height: auto;">
                    <?php the_title(); ?>  <!-- teacher's name is equal to the article title -->
                  </div>
                  <div class="teacher_mobile_line"></div>
                </div>
                <div class="profession mobile_top_content_container" style="height: 25vh;">
                  <div id="graduate">
                    <?php $teacher_education = get_field( "teacher_education" ); 
                      if( $teacher_education ){
                        echo $teacher_education;
                      }
                    ?>
                    <a target="_blank" href="<?php the_permalink(); ?>">
                    <img class="mobile_home_button" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_personalweb_dark_grey.svg">
                    </a>
                  </div>
                  <HR size="1px" color="#000">
                  <div id="t2" class="mobile_top_content" style="/*white-space: pre-line;*/ margin-top: 1vh;">
                    <?php $teacher_professional_area = get_field( "teacher_professional_area" ); 
                      if( $teacher_professional_area ){
                        echo $teacher_professional_area;
                      }
                    ?>
                  </div>
                </div>
                <div class="line" style="margin-top: 5vh;">
                  <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
                  <HR size="3px" color="#4F4F4F">
                </div>
              </a>
          </div>
          <?php
          $is_multiple=True;
            endwhile;
          else:?>
              <div class="poster-title">還沒有新增老師詳細內頁喔！</div>
          <?php
          endif;
          wp_reset_postdata();
          ?>
            

<!--    <div class="inblock" style="width:72.875vw; margin-bottom: 10vh;">
      <div class="teacher_block">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof劉紀蕙_photo.jpg" />
          </div>
          <span class="teacher">
            <div id="t1">講座教授兼所長</div>
            <div id="t2">劉紀蕙</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">美國伊利諾大學比較文學博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>精神分析與批判思想</li>
              <li>哲學：政治－美學－倫理</li>
              <li>台灣文學與文化研究</li>
              <li>東亞現代性思想</li>
              <li>視覺文化與跨藝術研究</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>

        <div class="teacher_block" style="float:right; margin-left: 1vw;">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof_linsufen_photo1" />
          </div>
          <span class="teacher">
            <div id="t1">教授</div>
            <div id="t2">林淑芬</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">英國艾嘉斯大學政治學博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>當代基進政治思想與思想史</li>
              <li>台灣戰後政治與經濟論述分析</li>
              <li>社會運動</li>
              <li>冷戰研究</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>
    </div>


    <div class="inblock" style="width:72.875vw; margin-bottom: 10vh;">
        <div class="teacher_block">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof朱元鴻_photo1.jpg" />
          </div>
          <span class="teacher">
            <div id="t1">教授</div>
            <div id="t2">朱元鴻</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">美國德州大學(奧斯汀)社會學博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>社會思想史</li>
              <li>當代社會理論</li>
              <li>文化研究</li>
              <li>都市民族誌</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>

        <div class="teacher_block" style="float:right; margin-left: 1vw;">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof藍弘岳_photo3.jpg" />
          </div>
          <span class="teacher">
            <div id="t1">教授</div>
            <div id="t2">藍弘岳</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">東京大學總合文化研究科博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>日本思想史</li>
              <li>日本漢學</li>
              <li>東亞思想文化交流史</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>
      </div>

      <div class="inblock" style="width:72.875vw; margin-bottom: 10vh;">
        
        <div class="teacher_block">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof邱德亮_photo1.jpg" />
          </div>
          <span class="teacher">
            <div id="t1">副教授</div>
            <div id="t2">邱德亮</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">法國高等社會科學院歷史與文明研究所博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>文化史比較研究</li>
              <li>品味理論</li>
              <li>飲食文化史</li>
              <li>當代法國思潮</li>
              <li>史學理論與歷史寫作</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>

        
        <div class="teacher_block" style="float:right; margin-left: 1vw;">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof彭明偉_photo2.jpg" />
          </div>
          <span class="teacher">
            <div id="t1">副教授</div>
            <div id="t2">彭明偉</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">清大中文系博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>中國現代</li>
              <li>當代文學</li>
              <li>台灣文學</li>
              <li>西洋文學思潮</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>
      </div>

      <div class="inblock" style="width:72.875vw; margin-bottom: 10vh;">
        
        <div class="teacher_block">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof莊雅仲_photo.png" />
          </div>
          <span class="teacher">
            <div id="t1">合聘教授</div>
            <div id="t2">蔡晏霖</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">美國杜克大學文化人類學博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>文化人類學理論</li>
              <li>民主與社會運動</li>
              <li>社區研究</li>
              <li>都市文化</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>


        
        <div class="teacher_block" style="float:right; margin-left: 1vw;">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof陳奕麟_photo1.png" />
          </div>
          <span class="teacher">
            <div id="t1">合聘教授</div>
            <div id="t2">陳奕麟</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">美國芝加哥大學人類學博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>文化研究</li>
              <li>社會理論</li>
              <li>歷史變遷</li>
              <li>論述實踐</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>
      </div>

      <div class="inblock" style="width:72.875vw; margin-bottom: 10vh;">
        
        <div class="teacher_block">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof蔡華臻_photo.jpg" />
          </div>
          <span class="teacher">
            <div id="t1">助理教授</div>
            <div id="t2">蔡華臻</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">美國芝加哥大學電影與媒體研究系博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>電影與媒體研究</li>
              <li>法蘭克福學派批判理論</li>
              <li>性別與酷兒理論</li>
              <li>移民與全球化</li>
              <li>當代藝術</li>
              <li>情感研究</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>


        
        <div class="teacher_block" style="float:right; margin-left: 1vw;">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof林文玲_photo.png" />
          </div>
          <span class="teacher">
            <div id="t1">合聘教授</div>
            <div id="t2">林文玲</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">德國哥廷根大學類學系哲學博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>文化人類學</li>
              <li>視覺人類學</li>
              <li>性別研究</li>
              <li>文化研究</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>
      </div>

      <div class="inblock" style="width:72.875vw; margin-bottom: 10vh;">
        
        <div class="teacher_block">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof蔡晏霖_photo1.png" />
          </div>
          <span class="teacher">
            <div id="t1">合聘副教授</div>
            <div id="t2">蔡晏霖</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">美國加州大學聖塔克魯茲校區人類學(性別研究)博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>文化人類學</li>
              <li>性別研究</li>
              <li>南亞區域研究</li>
              <li>族裔與離散</li>
              <li>食農研究</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>


        
        <div class="teacher_block" style="float:right; margin-left: 1vw;">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof王智明_photo1.jpg" />
          </div>
          <span class="teacher">
            <div id="t1">合聘副教授</div>
            <div id="t2">王智明</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">美國加州大學聖塔克魯茲分校文學系博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
              <li>跨國美國文學與文化</li>
              <li>文學理論與建制史</li>
              <li>亞洲現代思想</li>
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>
      </div>

      <div class="inblock" style="width:72.875vw; margin-bottom: 10vh;">
        
        <div class="teacher_block">
          <div class="photo">
          <img class = "teacher_img" src="<?php bloginfo('template_url'); ?>/images/professor/prof_Alain_Brossat_photo.jpg" />
          </div>
          <span class="teacher">
            <div id="t1">玉山學者/約聘研究員</div>
            <div id="t2">Alain Brossat</div>
            <div class="teacher_mobile_line"></div>
          </span>
          <span class="profession">
            <div id="graduate">巴黎第八大學社會學博士
            </div>
            <HR size="1px" color="#000">
            <ol class="v" style="margin-top:1.25vh; ">
            </ol>
          </span>
          <div class="line" style="margin-top:13.5vh;">
            <HR size="1px" color="#4F4F4F" style="margin-bottom:0.2vh;">
            <HR size="3px" color="#4F4F4F">
          </div>
        </div>
      </div>

    </div>

-->
  </div><!-- /.row -->
</div><!-- /.container -->



</?php get_template_part('includes/footer'); ?>