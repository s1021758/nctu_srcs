<?php
/*
 * Template Name: research-proposal
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <!-- 下拉式選單JS -->
  <script>
    function ShowDropDown(){
      if($(".button_style_noClick").css("display") == 'none'){
        $(".buttons").css("align-items","flex-start");
        $(".button_style_noClick").show();
      }
      else{
        closeDropDown();
      }
    }
    function closeDropDown(){
      if(screen.width<1024){
        $(".button_style_noclick").hide();
        $(".buttons").css("align-items","center");
      }
    }
  </script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(1)").addClass('nav_active');
      $ (".sidebarmenu a:eq(2)").removeClass('a_show');
      $ (".sidebarmenu a:eq(2)").addClass('sidebarmenu_active');

      function clean(){
        $(".planDiv_1").css("display","none");
        $(".planDiv_2").css("display","none");
        $(".planDiv_3").css("display","none");
        $("#btn_main").removeClass("button_style_plan1");
        $("#btn_main").addClass("button_style_noClick");
        $("#btn_other").removeClass("button_style_plan1");
        $("#btn_other").addClass("button_style_noClick");
        $("#btn_overall").removeClass("button_style_plan1");
        $("#btn_overall").addClass("button_style_noClick");
      }

      $("#btn_main").click(function(){
        clean();
        $("#btn_main").removeClass("button_style_noClick");
        $("#btn_main").addClass("button_style_plan1");
        $(".planDiv_1").css("display","inline-block");
        ShowDropDown();
        closeDropDown();
      });
      $("#btn_other").click(function(){
        clean();
        $("#btn_other").removeClass("button_style_noClick");
        $("#btn_other").addClass("button_style_plan1");
        $(".planDiv_2").css("display","inline-block");
        ShowDropDown();
        closeDropDown();
      });
      $("#btn_overall").click(function(){
        clean();
        $("#btn_overall").removeClass("button_style_noClick");
        $("#btn_overall").addClass("button_style_plan1");
        $(".planDiv_3").css("display","inline-block");
        ShowDropDown();
        closeDropDown();
      });


});

  </script>
  <style>
    .down_botton{
      display: none;
    }
    .title_block{
      display: block !important;
    }
    .title_container{
      display: none;
    }
    @media(max-width: 1024px){
      body{
        position: absolute;
        top: 0;
      }
      /*標題*/
      .title_block{
        display: block !important;
        height: auto !important;
        max-height: none !important;
        /*height: auto !important;*/
      }
      .title_container{
        display: flex;
        height: 13vh;
        width: 80vw;
        margin-right: 0;
        margin-bottom: 2.5vh;
        max-height: 34px;
      }
      .title_r{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 500 !important;
        font-size: 1.3em !important;
        letter-spacing: 0.1em !important;
        color: rgba(20, 20, 20, 1) !important;
        width: auto !important;
      }
      .mobile_title_lines{
        display: flex;
        margin-right: 2vw;
        width: auto;
        flex-grow: 1;
      }
      /*選單未完成 僅能顯示單一按鈕*/
      .buttons{
        display: block !important;
        width: auto !important; 
        height: 3.125vh;
        color: rgba(60, 0, 110, 0.8);
        border-radius: 3px;
        float:left !important;
        padding-bottom: 0.5vh;
      }
      .mobile_dropdown{
        display: inline-flex;
        flex-direction: column;
        z-index: 1;
        position: relative;
        top: -1vh;
      }
      #btn_main{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400;
        font-size: 1em;
        line-height: 3.125vh;
        letter-spacing: 0.1em;
        color: rgba(255, 255, 255, 1);
        float: left;
        padding: 0.25vh 2vw 0.25vh 2vw;
      }
      #btn_other{
        display: none;
      }
      #btn_overall{
        display: none;
      }
      .button_style_noclick{
        display: none;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1em !important;
        line-height: 3.125vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(255, 255, 255, 1) !important;
        background-color: rgba(60,0,110,0.8) !important;
        float: left !important;
        padding: 0.25vh 2vw 0.25vh 2vw !important;
        order: 2;
      }
      .down_botton{
        display: inline-block;
        padding-left: 2vw;
        height: 3.125vh;
        width: 3.125vh;
        max-height: 24px;
        max-width: 24px;
      }
      /*內文*/
      #planname_style{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 300 !important;
        font-size: 0.9em !important;
        line-height: 1.6em !important;
        letter-spacing: 0.1em !important;
        color: rgba(50, 50, 50, 1) !important;
      }
      .plan_detail{
        width: 80vw !important;
        border-bottom: none !important;
        padding: 0 !important;
        padding-top: 2.5vh !important;
        padding-bottom: 3vh !important;
      }
      .plan_block{
        display: none;
      }
      .plan_content{
        width:80vw;
        display: flex;
        justify-content: space-between;
      }
      .mobile-content{
        width: auto !important;
        margin-bottom: 1.5vh !important;
      }
      .mobile-time{
        display: none !important;
      }
      .mobile-person-name{
        direction: rtl;
        float: right;
        width: auto !important;
        margin-bottom: 1.5vh !important;
        min-width: 10vw;
      }
      .mobile_line{
        top:0;
      }
      .planDiv_2{
        position: initial !important;
      }
      .planDiv_3{
        position: initial !important;
      }
    }
    @media(min-width: 1025px){
      .title_block{
        display: none;
      }
    }
  </style>
</head>
<?php get_template_part('includes/phone-list'); ?>
<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar-research'); ?>


<body>


<div class="container">
  <div class="row">

    <div class="main">
      <div class="title_block" style="width:73vw; height:5vh;">
        <div class="title_container">
          <div class="title_r" style="width:10vw; float:left;">研究計畫</div>
          <div class="mobile_title_lines"></div>
          <div class="botton_container2">
            <a href="<?php echo site_url(); ?>/teachers-works/"> 
              <img class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
            </a>
            <img onclick="show_menu()" class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
          </div>
        
      </div>
        <div class="buttons" style="width:60vw; float:right;">
          <div class="mobile_dropdown">
            <div id="btn_overall" class="button_style_noClick">整合型研究或計畫</div>
            <div id="btn_other" class="button_style_noClick">其他學術單位委託研究計畫</div>
            <div id="btn_main" class="button_style_plan1">行政院國科會輔助專題研究計畫</div>
          </div>
          <img onclick="ShowDropDown()" class="down_botton"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_down_dark_grey.svg">


        </div>

      </div>
      <!--研究計畫內文-->

      <div class="planDiv_1" style="display: inline-block;">
        <div class="plan_block">
          <div id="planname_style" style="width:47vw;">專題研究計畫名稱</div>
          <div id="planname_style" style="width:13.5vw;">計畫期間</div>
          <div id="planname_style" style="width:9.7vw;">主持人/共同主持人</div>
        </div>

        <div class="plan_detail">
          <?php $research_proposal = get_post_meta( $post->ID, 'research_proposal', true );
              foreach( $research_proposal as $research_pro){?>
                <div class="plan_content">
                  <div id="planname_style" class="mobile-content" style="width:47vw; margin-bottom: 2vh;"><?php echo $research_pro['research_proposal_name']?></div>
                  <div id="planname_style" class="mobile-time" style="width:13.5vw; margin-bottom: 2vh;"><?php echo $research_pro['research_proposal_period']?></div>
                  <div id="planname_style" class="mobile-person-name" style="width:9.7vw; margin-bottom: 2vh;"><?php echo $research_pro['research_proposal_host']?></div>
                </div>
                <?php

              }?>
          <!-- <div class="plan_content">
            <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">江戶與明治日本的臺灣歷史書寫</div>
            <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">106/08/01-108/07/31</div>
            <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">藍弘岳</div>
          </div>
          <div class="plan_content">
            <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">戰後台灣文化菁英的社會學描述I：文學場域(1/2)</div>
            <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">106/08/01-108/07/31</div>
            <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">邱德亮</div>
          </div>
          <div class="plan_content">
            <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">穿行於革命、戰爭與城鄉之間：師陀與現代中國的歷史轉折(1/2)</div>
            <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">106/08/01-108/07/31</div>
            <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">彭明偉</div>
          </div>
          <div class="plan_content">
            <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">自由或合作？1950年代台灣的《自由中國》與《新社會》</div>
            <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">106/08/01-108/07/31</div>
            <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">林淑芬</div>
          </div> -->

        </div>
        <!--該線的多個div時位置不確定-->
        <hr class="mobile_line" />
      </div>
      

      <div class="planDiv_2" style="display: none; position: fixed;">
        <div class="plan_block">
          <div id="planname_style" style="width:47vw;">專題研究計畫名稱</div>
          <div id="planname_style" style="width:13.5vw;">計畫期間</div>
          <div id="planname_style" style="width:9.7vw;">主持人/共同主持人</div>
        </div>

        <div class="plan_detail">
          <?php $research_proposal_other = get_post_meta( $post->ID, 'research_proposal_other', true );
              foreach( $research_proposal_other as $research_other){?>
                <div class="plan_content">
                  <div id="planname_style" class="mobile-content" style="width:47vw; margin-bottom: 2vh;"><?php echo $research_other['research_proposal_name_other']?></div>
                  <div id="planname_style" class="mobile-time" style="width:13.5vw; margin-bottom: 2vh;"><?php echo $research_other['research_proposal_period_other']?></div>
                  <div id="planname_style" class="mobile-person-name" style="width:9.7vw; margin-bottom: 2vh;"><?php echo $research_other['research_proposal_host_other']?></div>
                </div>
                <?php

              }?>
        </div>
        <hr class="mobile_line" />
      </div>
      
      <div class="planDiv_3" style="display: none; position: fixed;">
        <div class="plan_block">
          <div id="planname_style" style="width:47vw;">專題研究計畫名稱</div>
          <div id="planname_style" style="width:13.5vw;">計畫期間</div>
          <div id="planname_style" style="width:9.7vw;">主持人/共同主持人</div>
        </div>

        <div class="plan_detail">
          <?php $research_proposal_overall = get_post_meta( $post->ID, 'research_proposal_overall', true );
              foreach( $research_proposal_overall as $research_overall){?>
                <div class="plan_content">
                  <div id="planname_style" class="mobile-content" style="width:47vw; margin-bottom: 2vh;"><?php echo $research_overall['research_proposal_name_overall']?></div>
                  <div id="planname_style" class="mobile-time" style="width:13.5vw; margin-bottom: 2vh;"><?php echo $research_overall['research_proposal_period_overall']?></div>
                  <div id="planname_style" class="mobile-person-name" style="width:9.7vw; margin-bottom: 2vh;"><?php echo $research_overall['research_proposal_host_overall']?></div>
                </div>
            <?php
            }
          ?>
        </div>
        <hr class="mobile_line" />
      </div>
      <!-- <div class="plan_detail">
        <div class="plan_content">
          <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">台灣左翼思想口述計畫（1970年代至1980年代）</div>
          <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">103/08/01-106/07/31</div>
          <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">陳光興</div>
        </div>
        <div class="plan_content">
          <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">共產主義理念重估：唯物辯證法與歷史發生學</div>
          <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">103/08/01-106/07/31</div>
          <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">劉紀蕙</div>
        </div>
        <div class="plan_content">
          <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">穿行於革命、戰爭與城鄉之間：師陀與現代中國的歷史轉折(1/2)</div>
          <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">104/08/01-105/07/31</div>
          <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">彭明偉</div>
        </div>
        <div class="plan_content">
          <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">「三代」想像與德川後期儒學政治思想⸺從徂徠後學到後期水戶學⸺</div>
          <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">103/08/01-105/07/31</div>
          <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">藍弘岳</div>
        </div>
        <div class="plan_content">
          <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">布爾迪厄《區判：品味判斷的社會批判》譯注計畫<br>(Pierre Bourdieu, 1979, La distinction: la critique social du judgment, Les Editions de Minuit)</div>
          <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">102/08/01-105/07/31</div>
          <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">邱德亮</div>
        </div>

      </div> -->


      <!-- <div class="plan_detail">
        <div class="plan_content">
          <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">「有問題的社會性：我們時代的自閉症鏡像」專書寫作計畫</div>
          <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">103/08/01-106/07/31</div>
          <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">陳光興</div>
        </div>
        <div class="plan_content">
          <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">荻生徂徠的「文學」與思想：「漢文脈」與「武國」知識的展開</div>
          <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">103/08/01-106/07/31</div>
          <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">劉紀蕙</div>
        </div>
        <div class="plan_content">
          <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">「漢學」與近代日本政治思想的展開和越境──「同文」想像與亞洲主義、國族主義的興起</div>
          <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">104/08/01-105/07/31</div>
          <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">彭明偉</div>
        </div>
        <div class="plan_content">
          <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">陳映真與台灣戰後左翼思想狀況：分斷、後殖民與死亡</div>
          <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">103/08/01-105/07/31</div>
          <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">藍弘岳</div>
        </div>
        <div class="plan_content">
          <div id="planname_style" style="width:47vw; margin-bottom: 2vh;">無與主權：疆界政治</div>
          <div id="planname_style" style="width:13.5vw; margin-bottom: 2vh;">102/08/01-105/07/31</div>
          <div id="planname_style" style="width:9.7vw; margin-bottom: 2vh;">邱德亮</div>
        </div>

      </div> -->


    </div>


  </div><!-- /.row -->
</div><!-- /.container -->
</body>

