<?php
/*
 * Template Name: homepage
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/flexslider.css" type="text/css" />
  <!-- flexslider header-->
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/homepage.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/jquery.flexslider.js"></script>
  <script src="https://use.typekit.net/hgf1mzq.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script>
  <!-- Place in the <head>, after the three links -->
  <script type="text/javascript" charset="utf-8">

  $(window).load(function() {
    var poster_title = [];
    var poster_date = [];
    var poster_place = [];

    <?php
      $is_multiple=False;
      $args = array(
      'category_name' => 'poster_horizontal_srcs',
      'posts_per_page' => 5
      );
      $the_query = new WP_Query($args);
      if($the_query->have_posts()):
          while($the_query->have_posts()):
              $the_query->the_post();
    ?>
        var newTitle = "<?php echo the_title();?>";  //海報文章標題
        var newDate = "<?php
            $poster_date = get_field( "poster_date" );
            if ( $poster_date ){
              echo $poster_date;
            }
            ?>";  //海報活動日期
        var newPlace = "<?php
            $poster_place_chi = get_field( "poster_place_chi" );  
             if ( $poster_place_chi){
              echo $poster_place_chi;
            }
            ?>";  //海報活動地點
        poster_title.push(newTitle);
        poster_date.push(newDate);
        poster_place.push(newPlace);
              
      <?php endwhile; ?>
    <?php endif; ?>


    $('.flexslider').flexslider({
      animation: "slide",
      controlNav: true,
      directionNav: true,
      after: function(slides){
        switch(slides.currentSlide){
            case 0: 
              $('#poster-title').html(poster_title[0]);
              $('.poster_time').html(poster_date[0]);
              $('.poster_place').html(poster_place[0]);
                break;
            case 1:
              $('#poster-title').html(poster_title[1]);
              $('.poster_time').html(poster_date[1]);
              $('.poster_place').html(poster_place[1]);
                break;
            case 2:
              $('#poster-title').html(poster_title[2]);
              $('.poster_time').html(poster_date[2]);
              $('.poster_place').html(poster_place[2]);
                break;
            case 3:
              $('#poster-title').html(poster_title[3]);
              $('.poster_time').html(poster_date[3]);
              $('.poster_place').html(poster_place[3]);
                break;
            case 4:
              $('#poster-title').html(poster_title[4]);
              $('.poster_time').html(poster_date[4]);
              $('.poster_place').html(poster_place[4]);
                break;
        }
    }
  });
});
</script>
  <style type="text/css">
    @media screen and (max-width: 1024px){
      .post-content {
        background-size: 100% 100%;
      }
      .menu-btn{
        float: right;
        width: 6.25vw;
        height: 6.25vw;
        margin-right:6vw;
        margin-top: 3.75vh; 
        opacity:0.5;
            }
      .flexslider_computer{
        display: none;
      }
      .flexslider{
        display: block !important;
        height: 50vw !important;
        margin-bottom: 0;
      }
      .slides{
        height: 50vw !important;
      }
      .slides img{
        height: 50vw !important;
      }
      .content{
        display: none;
      }
      #right_post{
        display: none;
      }
      .new_post{
        margin-top: 0 !important;
      }
      .textbox2{
        display: none;
      }
      #poster-title{
        display: none;
      }
      .button_style1{
        display: none;
      }
      .post-content hr{
        display: none;
      }


    }
    @media screen and (min-width: 1025px){
      .flexslider_mobile{
        display: none;
      }
    }
   
  </style>
</head>
<?php get_template_part('includes/homepage-phone-list'); ?>
<?php get_template_part('includes/header'); ?>
<?php //get_template_part('includes/header-en_header'); ?>
<?php //wp_head(); ?>

<body>
    <!--
    <div class="content">

      <span class="content_left"></span>
      <span class="content_right">
        <div class="textbox1">
          <p>國立陽明交通大學</p>
          <p>社會與文化研究所</p>
        </div>


<!--computer_poster_start-->
        <?php
          $is_multiple=False;
          $args = array(
          'category_name' => 'poster_horizontal_srcs',
          'posts_per_page' => 1
          );
          $the_query = new WP_Query($args);
          if($the_query->have_posts()):
              while($the_query->have_posts()):
                  $the_query->the_post();
        ?>

        <!-- dynamically change poster title and other info -->
        
        <p id="poster-title"> <?php echo the_title();?> </p>


        <HR size="1px" color="#ffe6a0">
        <div class="textbox2">
          <!-- poster time -->
          <p id="p_text" class="poster_time">
            <?php
            $poster_date = get_field( "poster_date" );
            if ( $poster_date ){
              echo $poster_date;
            }
            ?>
          </p>
          <!-- poster place -->
          <p id="p_text" class="poster_place">
            <?php
            $poster_place_chi = get_field( "poster_place_chi" );  
             if ( $poster_place_chi){
              echo $poster_place_chi;
            }
            ?>
          </p>
        </div>
        <a target="_blank" href="<?php the_permalink(); ?>" class="button_style1">繼續閱讀</a>
      </span>
        <?php endwhile; ?>
    <?php endif; ?>
    </div> -->
    <div class="flexslider_computer">
    <div class="flexslider">

      <ul class="slides" style="width:100vw; height:76vh;">
      <?php
      $is_multiple=False;
      $args = array(
      'category_name' => 'poster_horizontal_srcs',
      'posts_per_page' => 5
      );
      $the_query = new WP_Query($args);
      if($the_query->have_posts()):
          while($the_query->have_posts()):
              $the_query->the_post();
              if($is_multiple):
      ?>
      <?php endif; ?>
        <li>
          <a target="_blank" href="<?php the_permalink(); ?>">
            <?php 

        $image = get_field('poster_image');

        if( !empty($image) ): ?>

          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

        <?php endif; ?>
          </a>
        </li>

        <?php
        $is_multiple=True;
          endwhile;
        else:?>
            <div class="poster-title">還沒有發佈新活動海報喔！</div>
        <?php
        endif;
        wp_reset_postdata();
        ?>
      </ul>

    </div>
    </div>
<!--computer_poster_end-->



  <div class="post-content">

  </div>

<!--mobile_poster_start-->
    <div class="flexslider_mobile">
        <?php
          $is_multiple=False;
          $args = array(
          'category_name' => 'poster_horizontal_srcs',
          'posts_per_page' => 1
          );
          $the_query = new WP_Query($args);
          if($the_query->have_posts()):
              while($the_query->have_posts()):
                  $the_query->the_post();
        ?>

        <!-- dynamically change poster title and other info -->
        
        <p id="poster-title"> <?php echo the_title();?> </p>


        <HR size="1px" color="#ffe6a0">
        <div class="textbox2">
          <!-- poster time -->
          <p id="p_text" class="poster_time">
            <?php
            $poster_date = get_field( "poster_date" );
            if ( $poster_date ){
              echo $poster_date;
            }
            ?>
          </p>
          <!-- poster place -->
          <p id="p_text" class="poster_place">
            <?php
            $poster_place_chi = get_field( "poster_place_chi" );  
             if ( $poster_place_chi){
              echo $poster_place_chi;
            }
            ?>
          </p>
        </div>
        <a target="_blank" href="<?php the_permalink(); ?>" class="button_style1">繼續閱讀</a>
      </span>
        <?php endwhile; ?>
    <?php endif; ?>

    <div class="flexslider">

      <ul class="slides" style="width:100vw; height:100vh;">
      <?php
      $is_multiple=False;
      $args = array(
      'category_name' => 'poster_horizontal_srcs',
      'posts_per_page' => 5
      );
      $the_query = new WP_Query($args);
      if($the_query->have_posts()):
          while($the_query->have_posts()):
              $the_query->the_post();
              if($is_multiple):
      ?>
      <?php endif; ?>
        <li>
          <a target="_blank" href="<?php the_permalink(); ?>">
            <?php 

        $image = get_field('poster_image');

        if( !empty($image) ): ?>

          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

        <?php endif; ?>
          </a>
        </li>

        <?php
        $is_multiple=True;
          endwhile;
        else:?>
            <div class="poster-title">還沒有發佈新活動海報喔！</div>
        <?php
        endif;
        wp_reset_postdata();
        ?>
      </ul>

    </div>
  </div>
<!--mobile_poster_end-->


      <!-- left side block : srcs_news -->
      <div id="new_post" class="new_post" style="background-color: rgba(230,230,230,1); !important">
        <div class="menu-btn">
          <img onclick="show_menu(),close_newpost()" src="../wp-content/themes/nctu_srcs/images/mobile/btn/menu-button.svg">
        </div>  
        <div class="newtext"> 
          <p id="p_style">最新消息</p>
          <div class="bottom_line"></div>
            <a target="_blank" href="<?php bloginfo('template_url')?>/newslist" class="button_style2">更多消息</a>
        </div>
        <HR id="hr" size="1px" style="width:44.828125vw; margin-left:2vw;"/>
          <div class="news_block">
            <?php
            $is_multiple=False;
            $args = array(
            'category_name' => 'news_srcs',
            'posts_per_page' => 5,
            );
            $the_query = new WP_Query($args);
            if($the_query->have_posts()):
                while($the_query->have_posts()):
                    $the_query->the_post();
                    if($is_multiple):
            ?>
            <?php endif; ?>

            <!--every srcs_news block-->

            <div class="news_block_content"> 
              <div class="purple_block"></div>
              <div class="date">
                  <font style="font-size: 0.7em;line-height: 1.5vh; color: rgba(50, 50, 50, 1);"><?php the_time('Y'); ?></font>
                  <font style="font-size: 1em;line-height: 2.5vh; color: rgba(50, 50, 50, 1);"><?php the_time('m/d'); ?></font>
              </div>
                <a target="_blank" href="<?php the_permalink(); ?>"><p id="content_text"><?php the_title(); ?></p></a>
            </div>
              <?php
              $is_multiple=True;
                endwhile;
              else:?>
                  <div class="poster-title">還沒發佈新文章喔！</div>
              <?php
              endif;
              wp_reset_postdata();
              ?>
          </div> <!-- news_block -->
        </div> <!-- new_post -->

        <div id="right_post" class="new_post" style="float:right;">
          <!-- lecturer video -->
          <div class="video-container">
            <?php $video_link = get_field("video_link");
              echo $video_link;?>
          </div> <!-- video-container -->
        </div> <!-- right_post -->
    </div>



</body>

<?php get_template_part('includes/footer'); ?>
<?php //get_template_part('includes/footer-en_footer'); ?>
