<?php
/*
 * Template Name: thesis
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/student.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/table.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(5)").addClass('nav_active');
      $ (".sidebarmenu a:eq(1)").removeClass('a_show');
      $ (".sidebarmenu a:eq(1)").addClass('sidebarmenu_active');
});

  </script>
   <!-- js for mobile -->
  <script>
    $(document).ready(function(){
      if(screen.width<=1024){
        $("#semester").attr("disabled",true);
      }
    });
    function dropdown(){
      // 下拉選單安紐功能 
      var x = $("#semester").children("option").length;
      if(screen.width<=1024 && $("#semester").attr("size")==1){
        $("#semester").attr("disabled",false);
        $("#semester").attr("size",x);
        $("#semester").css("cssText","height:auto !important");
        $("#semester").append("<option value='' selected='selected'></option>");
        $("#semester").find(":selected").remove();
      }
      else if(screen.width<=1024 && $("#semester").attr("size")==x){
        $("#semester").attr("disabled",true);
        $("#semester").attr("size",1);
        $("#semester").css("cssText","height:3.125vh !important");
      }
      $("#semester").change(function(){
        if(screen.width<=1024){
          $("#semester").attr("disabled",true);
          $("#semester").attr("size",1);
          $("#semester").css("cssText","height:3.125vh !important");
        }
      });
    }
  </script>
  <style type="text/css">
    @media(max-width: 1024px){
      body{
        background-image: url("../wp-content/themes/nctu_srcs/images/cellphone_background.jpg");
        background-size: 100%;
      }
      .options{
        display: flex;
        position: relative;
        left:-59vw;
        width: auto !important;
        height: 3.125vh !important;
      }
      .options form{
        display: inline-block;
      }
      #semester{
        width: 16vw !important; 
        height: 3.125vh !important;
        border-radius: 3px !important;
        background: rgba(60, 0, 110, 0.8) !important;
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400 !important;
        font-size: 1.2em !important;
        line-height: 3.125vh !important;
        letter-spacing: 0.1em !important;
        color: rgba(255, 255, 255, 1) !important;
        margin-right: 2vw;
      }
      option{
        font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
        font-weight: 400;
        font-size: 1em;
        line-height: 3.125vh;
        letter-spacing: 0.1em;
        color: rgba(255, 255, 255, 1);
      }
      .button{
        display: inline-block;
        width:4vw;
        max-width: 24px;
      }
      .phone_main{
        height: 100vh;
      }
      .header{
        display: none;
      }
      .footer{
        display: none;
      }
      .sidebarmenu{
        display: none;
      }
      .main{
        display: none;
      }
      .thesis-title{
        width: 80vw;
        font-weight: 400;
        font-size: 1em;
        line-height: 1.5em;
        letter-spacing: 0.1em;
        color: rgba(50, 50, 50, 1);
        margin-top: 1.5vh;
      }
      .thesis-phone-title-1{
        float: left;
      }
      .thesis-line{
        float: left;
        margin-left: 1.75vw;
        margin-right: 2.5vw;
      }
      .phone_title_2{
        margin-bottom: 0vh;
        margin-top: 0vh;
        height: 2.5vh;
      }
      .rec{
        width: 16vw;
        height: 3.125vh;
        border-radius: 10px;
      }
      .phone_title_top{
        height: 2vh !important;
      }
      .phone_title_bottom{
        height: 0.8vh !important;
      }
      .title_r{
        float: left;
      }
      .title_block{
        margin-top: 11vh;
      }
    }
    @media(min-width: 1025px){
      .phone_main{
        display: none;
      }
    }
  </style>

</head>

<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar-student'); ?>
<?php get_template_part('includes/phone-list'); ?>


<body>

    <div class="main" style="width:71.875vw;">
      <div class="title_block" style=" width: 71.875vw;height: 4vh;">
        <div class="title_r" style="display: inline-block;width:25vw; float:left; height:2.75vh;">畢業論文</div>
        <div class="options" style="width:45vw; float:right; ">
          <select class="semester" style="    margin-right: 0;background: #e6e6e6 url('<?php bloginfo('template_url'); ?>/images/btn/arrowhead-pointing-down.png')  no-repeat; background-position: 10vw;">
            <option value="106_first_sem">106上</option>
          　<option value="105_second_sem">105下</option>
          　<option value="105_first_sem">105上</option>
            <option value="105_first_sem">104下</option>
            <option value="105_first_sem">104上</option>
            <option value="105_first_sem">103下</option>
            <option value="105_first_sem">103上</option>
          </select>
        </div>
      </div>
      <HR size="1px" style="margin-top:2.5vh; margin-bottom:2vh; width:71.825vw;">
      <a href="#" class="button_style_blue" style=" position: absolute;margin-bottom:1.25vh;color: rgba(255, 255, 255, 1);">碩士班</a>
      <div class="thesis-master" style="width: 71.875vw;margin-top: 8vh;">

        <?php $master_thesis = get_post_meta( $post->ID, 'master_thesis', true );
            foreach( $master_thesis as $masterthesis){?>
              <div class="thesis-block" style="height:10.75vh; margin-bottom:2.25vh;">
                <div class="thesis-name"><?php echo $masterthesis['master_thesis_name']?></div>
                <div class="thesis-title"><?php echo $masterthesis['master_thesis_topic']?></div>
                <div class="thesis-professor"><?php echo $masterthesis['master_thesis_professor']?></div>
              </div>
              <?php

            }?>


        <!-- <div class="thesis-block" style="height:7.75vh; margin-bottom:2.25vh;">
          <div class="thesis-name">麗麗
            (Margarita Shutova)</div>
          <div class="thesis-title">現代俄羅斯的光頭次文化：不同種類的表示
            Skinhead Movement in Modern Russia: Different Kinds of Representation
          </div>
          <div class="thesis-professor">司黛蕊 教授</div>
        </div>
        <div class="thesis-block" style="height:7.75vh; margin-bottom:2.25vh;">
          <div class="thesis-name">陳曼華</div>
          <div class="thesis-title">藝術與文化政治：戰後台灣藝術的主體形構
            Art and Cultural Politics: The Subject Formation of Taiwanese Art after World War Two
          </div>
          <div class="thesis-professor">顏娟英 教授</div>
        </div>
        <div class="thesis-block" style="height:7.75vh; margin-bottom:2.25vh;">
          <div class="thesis-name">劉雅芳</div>
          <div class="thesis-title">從冷戰聽覺轉向第三世界/亞洲的音樂生產:李雙澤、楊祖珺、黑名單工作室王明輝的音樂思想與實踐
            From Cold War Listening Experiences to Third World Music Production: Thinking Practice of LI Shuangze, YANG Tsuchuen and WANG Minghui
          </div>
          <div class="thesis-professor">陳光興 教授
            何東洪 教授</div>
        </div>
        <div class="thesis-block" style="height:7.75vh; margin-bottom:2.25vh;">
          <div class="thesis-name">楊成瀚</div>
          <div class="thesis-title">(不）令人讚賞的靈光對象：數位獨體化時代的藝術研究
            The (Un)Admirable Auratic Ob-ject: Study on the Art in the Age of Digital Individuation
          </div>
          <div class="thesis-professor">朱元鴻 教授
魏德驥 教授</div>
        </div> -->


      </div>


      <HR size="1px" style="margin-top:2.5vh; margin-bottom:2vh; width:1.2vw;">
      <a href="#" class="button_style_blue" style="background-color:rgba(180,5,0,1); position: absolute;margin-bottom:1.25vh;color: rgba(255, 255, 255, 1);">博士班</a>


        <div class="thesis-doctor" style="width: 71.875vw;height: 19vh;margin-top: 8vh;">

          <?php $doctor_thesis = get_post_meta( $post->ID, 'doctor_thesis', true );
            foreach( $doctor_thesis as $doctorthesis){?>
              <div class="thesis-block" style="height:10.75vh; margin-bottom:2.25vh;">
                <div class="thesis-name"><?php echo $doctorthesis['doctor_thesis_name']?></div>
                <div class="thesis-title"><?php echo $doctorthesis['doctor_thesis_topic']?></div>
                <div class="thesis-professor"><?php echo $doctorthesis['doctor_thesis_professor']?></div>
              </div>
              <?php

            }?>


          <!-- <div class="thesis-block" style="height:7.75vh; margin-bottom:2.25vh;">
            <div class="thesis-name">麗麗
              (Margarita Shutova)</div>
            <div class="thesis-title">現代俄羅斯的光頭次文化：不同種類的表示
              Skinhead Movement in Modern Russia: Different Kinds of Representation
            </div>
            <div class="thesis-professor">司黛蕊 教授</div>
          </div>
          <div class="thesis-block" style="height:7.75vh; margin-bottom:2.25vh;">
            <div class="thesis-name">陳曼華</div>
            <div class="thesis-title">藝術與文化政治：戰後台灣藝術的主體形構
              Art and Cultural Politics: The Subject Formation of Taiwanese Art after World War Two
            </div>
            <div class="thesis-professor">顏娟英 教授</div>
          </div>
          <div class="thesis-block" style="height:7.75vh; margin-bottom:2.25vh;">
            <div class="thesis-name">劉雅芳</div>
            <div class="thesis-title">從冷戰聽覺轉向第三世界/亞洲的音樂生產:李雙澤、楊祖珺、黑名單工作室王明輝的音樂思想與實踐
              From Cold War Listening Experiences to Third World Music Production: Thinking Practice of LI Shuangze, YANG Tsuchuen and WANG Minghui
            </div>
            <div class="thesis-professor">陳光興 教授
              何東洪 教授</div>
          </div>
          <div class="thesis-block" style="height:7.75vh; margin-bottom:2.25vh;">
            <div class="thesis-name">楊成瀚</div>
            <div class="thesis-title">(不）令人讚賞的靈光對象：數位獨體化時代的藝術研究
              The (Un)Admirable Auratic Ob-ject: Study on the Art in the Age of Digital Individuation
            </div>
            <div class="thesis-professor">朱元鴻 教授
  魏德驥 教授</div>
          </div> -->

        </div>
    </div>

<!--手機板-->    
    <div class="phone_main">
      <div class="title_block">
        <div class="title_r">學生論文</div>
        <div class="mobile_title_lines"></div>
        <div class="botton_container">
          <a href="<?php echo site_url(); ?>/rules/"> 
            <img class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
          </a>
          <img onclick="show_menu()" class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
          <a href="<?php echo site_url(); ?>/dissertation-award/"> 
            <img class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_right_dark_grey.svg">
          </a>
        </div>
      </div>        
      <div class="options" style="width:45vw; float:right; ">
        <form>
          <select size="1" class="semester" id="semester">
            <option value="106_first_sem">106上</option>
          　<option value="105_second_sem">105下</option>
          　<option value="105_first_sem">105上</option>
            <option value="105_first_sem">104下</option>
            <option value="105_first_sem">104上</option>
            <option value="105_first_sem">103下</option>
            <option value="105_first_sem">103上</option>
          </select>
        </form>
          <img onclick="dropdown()" class="button"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_down_dark_grey.svg">
      </div>

      <div class="clear"></div>
      <div class="phone_title_top"></div>
      <div>
        <div class="phone_title_2" >
          碩士班
        </div>
        <div class="phone_title_2_hr">
        </div>
        <div class="clear"></div>
      </div> 
      <div class="phone_title_bottom"></div>        
      <div class="phone_text">
        <?php $doctor_thesis = get_post_meta( $post->ID, 'doctor_thesis', true );
            foreach( $doctor_thesis as $doctorthesis){?>
              <div class="thesis-phone-title">
                <div class="thesis-phone-title-1"><?php echo $masterthesis['master_thesis_name']?></div>
                <div class="thesis-line">
                  |
                </div>
                <div class="thesis-phone-title-1"><?php echo $masterthesis['master_thesis_professor']?></div>
                <div class="thesis-title"><?php echo $masterthesis['master_thesis_topic']?></div>
              </div>
              <?php

            }?>
      </div>

      <div>
      <div class="phone_title_top"></div>
      <div>
        <div class="phone_title_2" >
          博士班
        </div>
        <div class="phone_title_2_hr">
        </div>
        <div class="clear"></div>
      </div> 
      <div class="phone_title_bottom"></div>  
      <div class="phone_text">
        <?php $doctor_thesis = get_post_meta( $post->ID, 'doctor_thesis', true );
            foreach( $doctor_thesis as $doctorthesis){?>
              <div class="thesis-phone-title">
                <div class="thesis-phone-title-1"><?php echo $doctorthesis['doctor_thesis_name']?></div>
                <div class="thesis-line">
                  |
                </div>
                <div class="thesis-phone-title-1"><?php echo $doctorthesis['doctor_thesis_professor']?></div>
                <div class="thesis-title"><?php echo $doctorthesis['doctor_thesis_topic']?></div>
              </div>
              <?php

            }?>
      </div>

    </div><!--phone_main end--> 
    
</body>



