<?php
/*
* Template Name: foreign
*/

?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/student.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(3)").addClass('nav_active');
      $ (".sidebarmenu a:eq(5)").removeClass('a_show')
      $ (".sidebarmenu a:eq(5)").addClass('sidebarmenu_active')
});

  </script>
  <style type="text/css">
    @media(max-width: 1024px){
      body{
        background-image: url("../wp-content/themes/nctu_srcs/images/cellphone_background.jpg");
        background-size: 100%;
      }
      .sidebarmenu{
        display: none;
      }
      .header{
        display: none;
      }
      .master-title{
        display: none;
      }
      .main{
        margin-top: 0vh !important;

      }
      .title_r{
        float: left; 
      }
      .title_block{
        margin-top: 11vh;
        padding-bottom: 0vh;
        margin-bottom: 2.5vh;
      }
      .background_image img{
        display: none;
      }
      .btn{
        border: 1px solid #000 !important;
        width: 11vw;
        height: 11vw;
        float: right;       
      }
      .btn-content span{
        color: rgba(0,0,0, 1) !important;
      }
      .overseas-text{
        background-color: rgba(0,0,0,0);
        padding-left: 0vw;
        padding-top: 0vh;
        z-index: 9999;
        height: 11vw !important;
        margin-top: 0vh !important;
        position: relative;
        float: none;
      }
      .overseas-title{
        display: none;
      }
      .overseas-subtitle{
        font-weight: 400;
        font-size: 1em;
        line-height: 1.6em;
        letter-spacing: 0.1em;
        color: rgba(50, 50, 50, 1);
      }
      .overseas-text hr{
        display: none;
      }
      .foreign_block{
        display: none;
      }
      .plan_detail{
        display: none;
      }
      .button_style_blue{
        width: 140px;
        height: 1vh;
        font-weight: 400;
        font-size: 1em;
        line-height: 2.5vh;
        color: rgba(255, 255, 255, 1);

      }
      .button_style_red{
        max-width: 80vw;
        height: auto;
        font-weight: 400;
        font-size: 1em;
        line-height: 2.5vh;
        color: rgba(180, 5, 0, 1);
        border-radius: 3px;
        margin-top: 3vh;

      }
      .plan_content{
        width: 80vw;
      }
      #foreign_style{
        width: 42vw;
        margin-right: 2vw;
        float: left;
        font-weight: 400;
        font-size: 1em;
        line-height: 1.6em;
        color: rgba(50, 50, 50, 1);
      }
      .foreign_style_2{
        width: 34vw;
        float: right;
        font-weight: 400;
        font-size: 1em;
        line-height: 1.6em;
        color: rgba(50, 50, 50, 1);
      }
      .clear_both{
        height: 1.5vh;
      }

    }
    @media(min-width: 1025px){
      .title_block{
        display: none;
      }
      .btn{
        border: 1px solid #fff;
        margin-right: 3vw;
      }
      .clear_both{
        display: none;
      }
      .phone{
        display: none;
      }

    }
    @media(max-width: 512px){
      .btn-content{
        font-size: 0.5em;
      }
      .overseas-subtitle{
        font-size: 0.8em;
      }
    }
  </style>   
</head>

<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar-student-recruitment'); ?>
<?php get_template_part('includes/phone-list'); ?>


    <div class="main" style="width:76vw;">

<!--phone title-->
      <div class="title_block">
        <div class="title_r">外籍生招生</div>
        <div class="mobile_title_lines"></div>
        <div class="botton_container2">
          <a href="<?php echo site_url(); ?>/chinese/"> 
            <img class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
          </a>
          <img onclick="show_menu()" class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
        </div>
      </div> 
      <div class="clear_both"></div>
<!--phone title end-->

      <div class="background_image">
        <img style="z-index:-1;" src="<?php bloginfo('template_url'); ?>/images/admissions_img_04.jpg" />
        <div class="overseas-text" style="height: 11vh; margin-top: -13.5vh;">
          <a target="_blank" href="http://www.ia.nctu.edu.tw/bin/home.php" class="btn btn-content">
            <span style="color: #fff;">交大
              國際處</span>
          </a>
          <div class="overseas-title">外籍生招生</div>
          <div class="overseas-subtitle">
            <?php $foreign_subtitle = get_field( "foreign_subtitle" );
              if( $foreign_subtitle ){
                echo $foreign_subtitle;
              }
            ?>
          </div>
          <HR size="1px" style="margin-top:2.75vh; margin-bottom:1.5vh; width:62.5vw; color:#fff;">
        </div>
      </div>

      <div class="foreign_block">
        <div id="foreign_style" style="width:21vw;"></div>
        <div id="foreign_style" style="width:17vw;">Entry to Fall Semester</div>
        <div id="foreign_style" style="width:24.5vw; font-size: 0.7em;">Entry to Spring Semester (Graduate programs only)</div>
        <HR size="1px" style="margin-top:2.75vh; margin-bottom:1.5vh; width:62.5vw;">
      </div>



      <div class="plan_detail">
        <div class="plan_content" style="height:2vh; margin-bottom:1.75vh;"> 
          <div id="foreign_style" style="width:21vw;">Application opens</div>
          <div id="foreign_style" style="width:17vw;">
            <?php $application_opens_fall = get_field( "application_opens_fall" );
              if( $application_opens_fall ){
                echo $application_opens_fall;
              }
            ?>
          </div>
          <div id="foreign_style" style="width:24.5vw;">
            <?php $application_opens_spring = get_field( "application_opens_spring" );
              if( $application_opens_spring ){
                echo $application_opens_spring;
              }
            ?>
          </div>
        </div>
        <div class="plan_content" style="margin-bottom:1.75vh;"> 
          <div id="foreign_style" style="width:21vw;">Application deadlines</div>
          <div id="foreign_style" style="width:17vw; white-space: pre-line;    margin-top: 6vh;">
            <?php $application_deadlines_fall = get_field( "application_deadlines_fall" );
              if( $application_deadlines_fall ){
                echo $application_deadlines_fall;
              }
            ?>
          </div>
          <div id="foreign_style" style="width:24.5vw;">
            <?php $application_deadlines_spring = get_field( "application_deadlines_spring" );
              if( $application_deadlines_spring ){
                echo $application_deadlines_spring;
              }
            ?>
          </div>
        </div>
        <div class="plan_content" style="margin-bottom:1.75vh;">
          <div id="foreign_style" style="width:21vw;">Announcement of Admission Result</div>
          <div id="foreign_style" style="width:17vw; white-space: pre-line;    margin-top: 6vh;">
            <?php $announcement_of_admission_result_fall = get_field( "announcement_of_admission_result_fall" );
              if( $announcement_of_admission_result_fall ){
                echo $announcement_of_admission_result_fall;
              }
            ?>
          </div>
          <div id="foreign_style" style="width:24.5vw;">
            <?php $announcement_of_admission_result_spring = get_field( "announcement_of_admission_result_spring" );
              if( $announcement_of_admission_result_spring ){
                echo $announcement_of_admission_result_spring;
              }
            ?>
          </div>
        </div>
        <div class="plan_content" style="margin-bottom:1.75vh;"> 
          <div id="foreign_style" style="width:21vw;">Classes start</div>
          <div id="foreign_style" style="width:17vw;">
            <?php $classes_start_fall = get_field( "classes_start_fall" );
              if( $classes_start_fall ){
                echo $classes_start_fall;
              }
            ?>
          </div>
          <div id="foreign_style" style="width:24.5vw;">
            <?php $classes_start_spring = get_field( "classes_start_spring" );
              if( $classes_start_spring ){
                echo $classes_start_spring;
              }
            ?>
          </div>
        </div>

    </div>
  <!--phone-->

    <div class="phone">
      <div class="button_style_blue" >Entry to Fall Semester</div>
      <div class="clear_both"></div>
      <div>
        <div class="plan_content"> 
          <div id="foreign_style">Application opens</div>
          <div class="foreign_style_2">
            <?php $application_opens_fall = get_field( "application_opens_fall" );
              if( $application_opens_fall ){
                echo $application_opens_fall;
              }
            ?>
          </div>
        </div>
        <div class="clear_both"></div>
        <div class="plan_content"> 
          <div id="foreign_style">Application deadlines</div>
          <div class="foreign_style_2">
            <?php $application_deadlines_fall = get_field( "application_deadlines_fall" );
              if( $application_deadlines_fall ){
                echo $application_deadlines_fall;
              }
            ?>
          </div>
        </div>
        <div class="clear_both"></div>
        <div class="plan_content">
          <div id="foreign_style">Announcement of Admission Result</div>
          <div class="foreign_style_2">
            <?php $announcement_of_admission_result_fall = get_field( "announcement_of_admission_result_fall" );
              if( $announcement_of_admission_result_fall ){
                echo $announcement_of_admission_result_fall;
              }
            ?>
          </div>
        </div>
        <div class="clear_both"></div>
        <div class="plan_content"> 
          <div id="foreign_style">Classes start</div>
          <div class="foreign_style_2">
            <?php $classes_start_fall = get_field( "classes_start_fall" );
              if( $classes_start_fall ){
                echo $classes_start_fall;
              }
            ?>
          </div>
        </div>

      </div>


        <div class="button_style_red" >Entry to Spring Semester (Graduate programs only)</div>
        <div class="clear_both"></div>
      <div>
        <div class="plan_content"> 
          <div id="foreign_style">Application opens</div>
          <div class="foreign_style_2">
            <?php $application_opens_spring = get_field( "application_opens_spring" );
              if( $application_opens_spring ){
                echo $application_opens_spring;
              }
            ?>
          </div>
        </div>
        <div class="clear_both"></div>
        <div class="plan_content"> 
          <div id="foreign_style">Application deadlines</div>
          <div class="foreign_style_2">
            <?php $application_deadlines_spring = get_field( "application_deadlines_spring" );
              if( $application_deadlines_spring ){
                echo $application_deadlines_spring;
              }
            ?>
          </div>
        </div>
        <div class="clear_both"></div>
        <div class="plan_content">
          <div id="foreign_style">Announcement of Admission Result</div>
          <div class="foreign_style_2">
            <?php $announcement_of_admission_result_spring = get_field( "announcement_of_admission_result_spring" );
              if( $announcement_of_admission_result_spring ){
                echo $announcement_of_admission_result_spring;
              }
            ?>
          </div>
        </div>
        <div class="clear_both"></div>
        <div class="plan_content"> 
          <div id="foreign_style">Classes start</div>
          <div class="foreign_style_2">
            <?php $classes_start_spring = get_field( "classes_start_spring" );
              if( $classes_start_spring ){
                echo $classes_start_spring;
              }
            ?>
          </div>
        </div>
      </div>

    </div>
  <!--phone_end-->
  </div>
