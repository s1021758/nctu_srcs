<?php
/*
 * Template Name: keynote-speech
 */
?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/student.css" type="text/css" />
  <!-- include mobile css -->
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(4)").addClass('nav_active');
      $ (".sidebarmenu a:eq(1)").removeClass('a_show');
      $ (".sidebarmenu a:eq(1)").addClass('sidebarmenu_active');
});

  </script>
  <style>
  .year-container{
    display: none;
  }
  .co-detail{
    white-space: normal;
  }
  @media(max-width: 1024px){
    .co_content{
      display: flex;
      flex-direction: row-reverse;
    }
    #co-date{
      margin:0 !important;
      font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
      font-weight: 400;
      font-size: 1em;
      line-height: 1.6em;
      letter-spacing: 0.1em;
      color: rgba(50, 50, 50, 1);
    }
    .year-container{
      display: block;
      height: 3.125vh;
      padding-bottom: 0.5vh;
    }
    .year-block{
      display: inline-block;
      width: 15.5vw; 
      height: 3.125vh;
      border-radius: 3px;
      background-color: rgba(60, 0, 110, 0.8);
      margin-right: 2vw;
    }
    .year-font{
      font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
      font-weight: 400;
      font-size: 1.2em;
      line-height: 3.125vh;
      letter-spacing: 0.1em;
      color: rgba(255, 255, 255, 1);
      float:left;
    }
    .year-botton{
      max-height: 24px;
      max-width: 24px;
      height: 3vh;
      width:3vh;
      padding-top: 0.0625vh;
      padding-bottom: 0.0625vh;
    }
    .mobile-word-container{
      display: flex !important;
      flex-direction: column;
      width: 67vw;
    }
    .mobile-word-container div:nth-of-type(1){
      order:2;
    }
    .mobile-word-container div:nth-of-type(2){
      order:1;
    }
    .mobile-word-container hr:nth-of-type(1){
      order:4;
    }
    #english-font{
      font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
      font-weight: 600;
      font-size: 1.25em;
      line-height: 1.5em;
      color: rgba(50, 50, 50, 1);
      margin-bottom: 1vh;
    }
    #chinese-font{
      font-family: 'Noto Serif CJK TC', 'Noto Serif CJK', 'Source Han Serif TC', 'Source Han Serif', source-han-serif-sc, serif;
      font-weight: 400;
      font-size: 1em;
      line-height: 1.6em;
      letter-spacing: 0.1em;
      color: rgba(50, 50, 50, 1);
    }
    .mobile_line{
      top:1.5vh;
    }
  }
  </style>
</head>
<?php get_template_part('includes/phone-list'); ?>
<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar-global-lounge'); ?>



<body>

    <div class="main">
      <div class="title_block" style="width:72vw; height:5vh;">
        <div class="title_r" style="width:25vw; float:left; height:2.75vh;">專題演講</div>
        <div class="mobile_title_lines"></div>
          <div class="botton_container">
            <a href="<?php echo site_url(); ?>/academic-cooperation/"> 
              <img class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_left_dark_grey.svg">
            </a>
            <img onclick="show_menu()" class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
            <a href="<?php echo site_url(); ?>/workshop/"> 
              <img class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_right_dark_grey.svg">
            </a>
          </div>
      </div>
      <div class="year-container">
        <div class="year-block">
          <div class="year-font">
            2017
          </div>
        </div>
        <img class="year-botton"src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_down_dark_grey.svg">
      </div>
      <div class="co_block" style="width:72vw; padding: 1vh 0vw; border-bottom: #000 solid 1px;">
        <div id="co-title" style="width:8vw;">時間</div>
        <div id="co-title" style="width:35vw;">主講人</div>
        <div id="co-title" style="width:27.875vw;">講題</div>

      </div>

        <?php $keynote_speech = get_post_meta( $post->ID, 'keynote_speech', true );
            foreach( $keynote_speech as $keynote){?>
            <div id="mobile_content_box" class="co_detail" style="width:72vw;">
              <div class="co_content" style=" border-bottom: #000 solid 1px; padding-top: 1vh;">
                <div id="co-date" class="co-detail" style="width:6vw; margin-left:1vw; margin-right:2vw;"><?php echo $keynote['speech_date']?></div>
                <div class="mobile-word-container" style="display: inline-block;">
                  <div id="chinese-font" class="co-detail" style="width:32vw;"><?php echo $keynote['speech_hosts']?></div>
                  <div id="english-font" class="co-detail" style="width:25.375vw;"><?php echo $keynote['speech_topic']?></div>
                  <hr class="mobile_line" />
                </div>
              </div>
            </div>
              <?php

            }?>

        <!-- 最新消息中屬於專題演講的區塊 -->
        <?php 
        // wp query
        $args = array(
          'category_name' => 'note_speech_srcs',
          'orderby' => 'date',
          'order'   => 'DESC',
        );
        $the_query = new WP_Query($args);
        if($the_query->have_posts()):
          while($the_query->have_posts()):
            $the_query->the_post();
        ?>
            <div id="mobile_content_box" class="co_detail" style="width:72vw;">
              <div class="co_content" style=" border-bottom: #000 solid 1px; padding-top: 1vh;">
                <?php $speech_date = get_field('speech_date');
                if( !empty($speech_date )): ?>
                <div id="co-date" class="co-detail" style="width:6vw; margin-left:1vw; margin-right:2vw;"><?php echo $speech_date;?></div>
              <?php endif; ?>
                <div class="mobile-word-container" style="display: inline-block;">
                  <?php $speech_host = get_field('speech_host');
                  if( !empty($speech_host)): ?>
                  <div id="chinese-font" class="co-detail" style="width:32vw;"><?php echo $speech_host;?></div>
                <?php endif; ?>
                <?php $speech_topic = get_field('speech_topic');
                 if( !empty($speech_topic)): ?>
                  <div id="english-font" class="co-detail" style="width:25.375vw;" >
                    <a href="<?php the_permalink(); ?>">
                    <?php echo $speech_topic;?>
                    </a>
                  </div>
                <?php endif; ?>
                  <hr class="mobile_line" />
                </div>
              </div>
            </div>
          <?php endwhile; endif; ?>

      <!-- <div class="co_detail" style="width:72vw;">
        <div class="co_content" style=" border-bottom: #000 solid 1px; padding-top: 1vh;">
          <div class="co-detail" style="width:6vw; margin-left:1vw; margin-right:2vw;">2017.6.6</div>
          <div class="co-detail" style="width:32vw;">磯前順一教授，京都日本研究國際中心教授</div>
          <div class="co-detail" style="width:25.375vw; margin-right:1vw;">Conflict, Justice and Decolonization Lecture & Workshop Series：6/6(二)</div>
        </div>
      </div> -->

      <!-- <div class="co_detail" style="width:72vw;">
        <div class="co_content" style=" border-bottom: #000 solid 1px; padding-top: 1vh;">
          <div class="co-detail" style="width:6vw; margin-left:1vw; margin-right:2vw;">2017.6.2</div>
          <div class="co-detail" style="width:32vw;">
              <p>Sandro Mezzadra，義大利波隆那大學政治與社會科學系副教授</p>
              <p>Jon Solomon 蘇哲安，法國里昂大學教授</p>
              <p>Alain Brossat，巴黎八大、交大社文所客座教授</p>
              <p>板垣?太 Ryuta Itagaki，日本同志社大學社會學系教授</p>
              <p>磯前順一 Jun’ichi Isomae，日本京都日本研究國際中心教授</p>
              <p>井上間?文 Mayumo Inoue，日本一橋大學語言與社會研究所副教授</p>
              <p>酒井直樹 Naoki Sakai，康乃爾大學東亞研究╱比較文學系教授</p>
              <p>林國偉 Benny Lim，香港中文大學文化及宗教研究系助理教授</p>
              <p>鍾佩琦 Peichi Chung，香港中文大學文化及宗教研究系助理教授</p>
              <p>陳佩甄 Eno Pei Jean Chen，中央研究院博士後研究員</p>
              <p>汪俊彥 Chunyen Wang，台灣師範大學台灣語文學系助理教授</p>
              <p>王威智 Wei-Chih Wang，清華大學天下書院導師</p>
          </div>
          <div class="co-detail" style="width:25.375vw; margin-right:1vw;">Conflict, Justice and Decolonization Lecture & Workshop
Series：6/2-3 Paradigm Shift of the Colonial-Imperial Order
and the Aporia of Human Sciences</div>
        </div>
      </div>

      <div class="co_detail" style="width:72vw;">
        <div class="co_content" style=" border-bottom: #000 solid 1px; padding-top: 1vh;">
          <div class="co-detail" style="width:6vw; margin-left:1vw; margin-right:2vw;">2017.5.17</div>
          <div class="co-detail" style="width:32vw;">謝尚伯，澳洲國立大學亞太學院博士</div>
          <div class="co-detail" style="width:25.375vw; margin-right:1vw;">民主轉型國家的改革與公民社會: 印尼經驗的啟示</div>
        </div>
      </div>

      <div class="co_detail" style="width:72vw;">
        <div class="co_content" style=" border-bottom: #000 solid 1px; padding-top: 1vh;">
          <div class="co-detail" style="width:6vw; margin-left:1vw; margin-right:2vw;">2017.5.12</div>
          <div class="co-detail" style="width:32vw;">
            <p>藤谷藤隆 Takashi Fujitani，多倫多大學東亞研究/歷史系教授</p>
            <p>米山麗莎 Lisa Yoneyama，多倫多大學東亞研究/婦女與性別研究學系教授</p>
            <p>坪井秀人 Hideto Tsuboi，京都日本研究國際中心日本文學教授</p>
          </div>
          <div class="co-detail" style="width:25.375vw; margin-right:1vw;">Conflict, Justice and Decolonization Lecture & Workshop
Series：5/12 The Legacies of Pax Americana and the Remnants
of American Colonial-Empire</div>
        </div>
      </div>

      <div class="co_detail" style="width:72vw;">
        <div class="co_content" style=" border-bottom: #000 solid 1px; padding-top: 1vh;">
          <div class="co-detail" style="width:6vw; margin-left:1vw; margin-right:2vw;">2017.5.10</div>
          <div class="co-detail" style="width:32vw;">
            <p>藤谷藤隆 Takashi Fujitani，多倫多大學東亞研究/歷史系教授</p>
            <p>米山麗莎 Lisa Yoneyama，多倫多大學東亞研究/婦女與性別研究學系教授</p>

          </div>
          <div class="co-detail" style="width:25.375vw; margin-right:1vw;">Conflict, Justice and Decolonization Lecture & Workshop
Series</div>
        </div>
      </div>

      <div class="co_detail" style="width:72vw;">
        <div class="co_content" style=" border-bottom: #000 solid 1px; padding-top: 1vh;">
          <div class="co-detail" style="width:6vw; margin-left:1vw; margin-right:2vw;">2017.5.4</div>
          <div class="co-detail" style="width:32vw;">李雪莉， 《報導者》 總主筆</div>
          <div class="co-detail" style="width:25.375vw; margin-right:1vw;">看不見的奴役 — 從〈血淚漁場〉調查報導談起，Revealing
the Hidden Modern Day Slavery</div>
        </div>
      </div>

      <div class="co_detail" style="width:72vw;">
        <div class="co_content" style=" border-bottom: #000 solid 1px; padding-top: 1vh;">
          <div class="co-detail" style="width:6vw; margin-left:1vw; margin-right:2vw;">2017.5.4</div>
          <div class="co-detail" style="width:32vw;">土井敏邦（Toshikuni Doi）導演，日本獨立記者</div>
          <div class="co-detail" style="width:25.375vw; margin-right:1vw;">What Is Going On in Gaza, Palestine</div>
        </div>
      </div>

      <div class="co_detail" style="width:72vw;">
        <div class="co_content" style=" border-bottom: #000 solid 1px; padding-top: 1vh;">
          <div class="co-detail" style="width:6vw; margin-left:1vw; margin-right:2vw;">2017.5.4</div>
          <div class="co-detail" style="width:32vw;">Walter Mignolo</div>
          <div class="co-detail" style="width:25.375vw; margin-right:1vw;">Professor Walter Mignolo 「殖民、去殖民、資本主義」系列</div>
        </div>
      </div> -->

    </div>
</body>
