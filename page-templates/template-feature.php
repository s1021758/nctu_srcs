<?php
/*
* Template Name: feature
*/

?>

<head>
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/student.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/mobile-css/Tu-frame-mobile.css" type="text/css" />  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>

    $( document ).ready(function() {
      $ ("#top-menu a:eq(3)").addClass('nav_active');
      $ (".sidebarmenu a:eq(0)").removeClass('a_show')
      $ (".sidebarmenu a:eq(0)").addClass('sidebarmenu_active')
});

  </script>
  <style type="text/css">
    @media(max-width: 1024px){
      body{
        background-image: url("../wp-content/themes/nctu_srcs/images/cellphone_background.jpg");
        position: absolute;
        top: 0;
        height: 160%;
      }
      .sidebarmenu{
        display: none;
      }
      .header{
        display: none;
      }
      .master-title{
        display: none;
      }
      .main{
        margin-top: 0vh !important;

      }
      .title_r{
        float: left;
      }
      .title_block{
        margin-top: 11vh;
        padding-bottom: 0vh;
        margin-bottom: 2.5vh;
      }
      .feature-title{
        display: none;
      }
      .hr1{
        display: none;
      }
      .feature-content{
        width: 80vw;
        font-weight: 400;
        font-size: 1em;
        line-height: 1.6em;
        letter-spacing: 0.1em;
        color: rgba(50, 50, 50, 1);
        margin-bottom: 2vh;
        margin-top: 3vh !important;
      }
      #features{
        margin-top: 5vh !important;
        font-weight: 400;
        font-size: 1em;
        line-height: 1.6em;
        letter-spacing: 0.1em;
        color: rgba(50, 50, 50, 1);
      }
      .video_block{
        float: none !important;
        width: 80vw !important;
      }
      .video-title{
        margin-top: 5vh !important;
        font-weight: 400;
        font-size: 1em;
        line-height: 1.6em;
        letter-spacing: 0.1em;
        color: rgba(50, 50, 50, 1);        
      }
      .video-container {
        position: relative;
        padding-bottom: 56.25%;
        padding-top: 30px;
        height: 0;
        overflow: hidden;
        margin-top: 0 !important;
      }

      .video-container iframe, .video-container object, .video-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }
      .phone_botton{
        height: 15vh;
      }
      .video-title{
        display: none;
      }
      .phone-video-title{
        font-weight: 400;
        font-size: 1.3em;
        line-height: 1.6em;
        letter-spacing: 0.1em;
        color: rgba(50, 50, 50, 1); 
        margin-top: 3vh;
        text-align: center; 
      }
    }
    @media(min-width: 1025px){
      .title_block{
        display: none;
      }
      .hr2{
        display: none;
      }
      .phone_botton{
        display: none;
      }
      .phone-video-title{
        display: none;
      }
      .feature-content{
        margin-bottom: 0vh;
      }

    }

  </style>      
</head>

<?php get_template_part('includes/header'); ?>
<?php get_template_part('includes/sidebar-student-recruitment'); ?>
<?php get_template_part('includes/phone-list'); ?>

    <div class="main" style="width:71.5vw;">

<!--phone title-->
          <div class="title_block">
            <div class="title_r">社文所特色</div>
            <div class="mobile_title_lines"></div>
            <div class="botton_container2">
              <img onclick="show_menu()" class="botton1" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_stop_dark_grey.svg">
              <a href="<?php echo site_url(); ?>/master/"> 
                <img class="botton2" src="../wp-content/themes/nctu_srcs/images/mobile/btn/btn_right_dark_grey.svg">
              </a>
            </div>
          </div> 
          <div class="clear_both"></div>
<!--phone title end-->     

      <div class="f_block">
        <div class="feature-title">特色</div>
        <HR class="hr1" size="1px" style="margin-bottom:1.25vh;">
          <div id="features" style="margin-top: 2vh;">
            <?php $feature_list = get_field( "feature_list" );
            if( $feature_list ){
              echo $feature_list;
            }
            ?>
          </div>
        <HR class="hr2" size="1px" style="margin-top:1.25vh;">
        <div class="feature-content" style="margin-top:5vh;">
          <?php $feature_content = get_field( "feature_content" );
            if( $feature_content ){
              echo $feature_content;
            }
            ?>
        </div>
      </div>
      <HR class="hr2" size="1px" style="margin-top:1.25vh;">
      <div class="phone-video-title">
        <?php $video_name1 = get_field( "video_name1" );
          if( $video_name1 ){
              echo $video_name1;
          }
        ?>        
      </div>
      <div class="video_block video-container" style="margin-right:8.5vw; margin-top: 5vh; float:left; width:31.25vw; margin-bottom:5vh;">
        <div class="video-title">
          <?php $video_name1 = get_field( "video_name1" );
            if( $video_name1 ){
              echo $video_name1;
            }
            ?>
        </div>
            <?php $video_link1 = get_field( "video_link1" );
              echo $video_link1;
            ?>

      </div>
      <HR class="hr2" size="1px" style="margin-top:1.25vh;">
      <div class="phone-video-title">
        <?php $video_name2 = get_field( "video_name2" );
          if( $video_name2 ){
              echo $video_name2;
          }
        ?>        
      </div>
      <div class="video_block video-container" style="margin-top: 5vh; width:31.25vw; display:inline-block;">
        <div class="video-title">
          <?php $video_name2 = get_field( "video_name2" );
            if( $video_name2 ){
              echo $video_name2;
            }
            ?>
        </div>
            <?php $video_link2 = get_field( "video_link2" );
              echo $video_link2;
            ?>
      </div>
      <HR class="hr2" size="1px" style="margin-top:5vh;">




    </div>
