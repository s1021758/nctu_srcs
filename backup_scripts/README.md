# nctu_ac_backup

把網站資料庫及 wordpress 使用者檔案內容（wp-content）壓縮後備份至 AWS S3 或 Glacier 的工具

## 準備工作

### 1.在要備份的主機上載到這個 repository
### 2.安裝 pip3 (lightsail 可能要用 get-pip.py 裝)
### 3.在這個專案目錄下 `pip3 install -r requirements.txt`
### 4.建立名為 credentials 的檔案，內容如下：
```
DB_USER="<資料庫使用者>"
DB_PASS="<資料庫密碼>"
DB_HOST="<資料庫主機位置>:<port>"
+ MYSQL_PATH="/usr/bin"
```

### 5.安裝 awscli
### 6.用 `aws configure` 設定 AWS 認證所需的 key

## 執行備份

在這個專案目錄下 `./backup.sh s3 <htdocs路徑>` 或 `./backup.sh glacier <htdocs路徑>`

## 定期備份

這個專案的定期備份使用 crontab 實作，就依照 crontab 的格式定期下執行備份的指令即可（注意 working directory）

## 取得備份

### S3

可以在 AWS S3 Console 直接看到檔案。

### Glacier

可以先用 awscli 得到 vault inventory ，再依照 inventory 列出的 ArchiveId 呼叫 glacier 的 API 。詳情參閱 [這裡](https://docs.aws.amazon.com/amazonglacier/latest/dev/retrieving-vault-inventory-cli.html) 和 [這裡](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/glacier.html#Glacier.Archive.initiate_archive_retrieval)

兩個步驟都需要等，時間從幾分鐘到近一天不等。

## 刪除備份

S3 可以在 console 上刪，也可以設定 object lifecycle 讓檔案自動 expire 。
 
Glacier 設計上就不是要讓人頻繁刪除檔案的，真的要刪一樣是先取得 vault inventory 之後依照 ArchiveId 呼叫 Glacier 的 API 。


## 作者
2020 Austin 撰寫完整程式、文件
2021 Sean 修改
