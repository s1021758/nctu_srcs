#!/usr/bin/env bash
cd "$(dirname "$0")" || exit

if [[ $# -ne 2 ]]; then
    echo "Usage: ./backup.sh <s3|glacier> <path to htdocs>"
    exit
fi

# Assume the base to be at wordpress document root
STORAGE=$1
BASE=$2
DB_DUMP="/tmp/db.sql"
CONTENT_DIR="${BASE}/wp-content"

# Load db credentials
source ./credentials

# Dump the database
mysqldump -h $DB_HOST \
    -u $DB_USER \
    --password=$DB_PASS \
    --all-databases \
    --add-drop-database > "$DB_DUMP"

# Compress database, site contents and config
DATETIME="$(date --iso-8601='second')"
ARCHIVE="/tmp/nctu-database-backup-$DATETIME.tar.gz"
# tar --ignore-failed-read -zcvf $ARCHIVE \
tar zcvf "$ARCHIVE" \
    --exclude ".git" \
    "$CONTENT_DIR" \
    "${BASE}/wp-config.php" \
    "${BASE}/.htaccess" \
    "$DB_DUMP"

# Put the archive into the glacier/s3
python3 backup.py "$STORAGE" "$ARCHIVE" \
&& rm -f "$ARCHIVE"
