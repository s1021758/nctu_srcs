import boto3
import sys, os

if __name__ == '__main__':
    service = sys.argv[1]
    backupPaths = sys.argv[2:]
    client = boto3.client(service)
    for backupPath in backupPaths:
        if service == 'glacier':
            with open(backupPath, 'rb') as f:
                response = client.upload_archive(
                    vaultName='nctu-srcs',
                    body=f
                )
                print(response)
        elif service == 's3':
            archiveName = os.path.basename(backupPath)
            client.upload_file(
                backupPath,
                'nctu-srcs',
                ("backup/%s" % archiveName)
            )
